mvcApp = angular.module('myApp', ['ngMaterial']);



(function () {

//----------------------------------------------------------
    mvcApp.config([], function(){ 
       console.log('config started');
      
     });

//----------------------------------------------------------
    mvcApp.controller('ngCtrl_main', function ($scope, $state, $mdSidenav, $element) { 
       console.log('controller started');

        $scope.MyDateNuo = new Date();
        $scope.MyDateIki = new Date();
        $scope.TextInToolbar = "";

        $scope.MobIrenginiai = [
             { ID:'Samsung SIII, 02345023948520', Info:'registruotas Jono Jonaičio, 2016.04.01'},
             { ID:'iPhone 5, 203480348990-909008085209', Info:'registruotas Petro Petraičio, 2016.01.01'},
             { ID:'Samsung SII, 01345023948520', Info:'registruotas Jono Jonaičio, 2016.04.01'},
             { ID:'Samsung SI, 52345023956320', Info:'registruotas Petro Jonaičio, 2016.04.01'},
             { ID:'Samsung s, 0124503148520', Info:'registruotas Dovydo Jonaičio, 2016.04.01'},
             { ID:'Samsung mini, 76312343948520', Info:'registruotas Mato Jonaičio, 2016.04.01'}
        ];

        //---clicked side nav
        $scope.clickedSideNav = function(sMenu) {
            $scope.SideNavToggle();
            if (sMenu=='statistics') {
                $state.go('state1');
                $scope.TextInToolbar = sMenu;
            }
            if (sMenu=='devices') {
                $state.go('state2');
                $scope.TextInToolbar = sMenu;
            }
            if(sMenu=='logout') { 
                $state.go('login');
            }
            if(sMenu=='statistics') { 
                drawChart();
            }
        };

        //========================= main layout
        $scope.SideNavToggle = function() {
            $mdSidenav('left').toggle();
        };


    });

//----------------------------------------------------------
    mvcApp.run([function () { 
      console.log('run started!');
    }]);

})();


function drawChart() {
    //========================= chart
    var data = {
        labels: ['0701','0702','0703','0704','0705','0706'],
        datasets: [{
                label: "-",
                fillColor: "rgba(151,187,205,0.2)",
                strokeColor: "rgba(151,187,205,1)",
                pointColor: "rgba(151,187,205,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(151,187,205,1)",
                data: [200,150,250,100,270,250]
            }]
    };

    var ctx = document.getElementById("stChart").getContext("2d");
    var oStChart = new Chart(ctx).Line(data);
};
