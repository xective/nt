#@(#) script.ddl

DROP TABLE IF EXISTS User;
CREATE TABLE User
(
	id int,
	username varchar (255),
	password varchar (255),
	email varchar (255),
	phone varchar (255),
	id_User integer,
	PRIMARY KEY(id_User)
);
