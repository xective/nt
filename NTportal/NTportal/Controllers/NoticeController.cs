﻿using NTportal.Data.DatabaseContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NTportal.BusinessLogic.Interfaces;
using NTportal.BusinessLogic;
using NTportal.Models.ViewModels;
using NTportal.Data.Models;
using NTportal.Data.Entities;

namespace NTportal.Controllers
{
    public class NoticeController : Controller
    {
        private readonly INoticeService NoticeService;
        private readonly IAddressService AddressService;
        private readonly IBuildingService BuildingService;
        private readonly IRepository Repository;
        public NoticeController(INoticeService ns, IRepository rp, IAddressService ads, IBuildingService bs)
        {
            NoticeService = ns;
            Repository = rp;
            AddressService = ads;
            BuildingService = bs;
        }
        //private MySqlDbContext db = new MySqlDbContext();
        [HttpGet]
        [AllowAnonymous]
        public ViewResult Index(bool isASC = true, int count = 20)
        {
            var notices = Repository.GetNotices(isASC, count);
            var viewModel = new NoticesViewModel { Notices = notices.Cast<NoticeViewModel>().ToList() };
            return View(viewModel);
        }
        [HttpPost]
        public JsonResult GetNewest()
        {
            var notices = Repository.GetNewest();
            return notices;
        }

        [HttpPost]
        public int GetNewestId()
        {
            var notice = Repository.GetNewestNoticeId();
            return notice;
        }

        [HttpPost]
        public int GetNewestAddressId()
        {
            var address = Repository.GetNewestAddressId();
            return address;
        }

        public int GetUsersListCount(int id)
        {
            var count = Repository.GetUsersListCount(id);
            return count;
        }
        public JsonResult GetUsersList(int id,int page)
        {
            page = page - 1;
            if (page < 0)
            {
                page = 0;
            }
            var notices = Repository.GetUsersList(id, page);
            return notices;
        }
        [HttpPost]
        public JsonResult GetNotice(int id)
        {
            var notices = Repository.GetNotice(id);
            return notices;
        }
        [HttpGet]
        public JsonResult GetList(FilterViewModel filter,int page)
        {
            page = page - 1;
            if (page < 0)
            {
                page = 0;
            }
            var notices = Repository.GetNoticesFiltered((FilterModel)filter,page);
            return notices;
            //return new JsonResult() { Data = notices, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpPost]
        public JsonResult GetMyList(string Email,int page)
        {
            var notices = Repository.GetMyList(Email,page-1);
            return notices;
            //return new JsonResult() { Data = notices, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpPost]
        public int GetListCount(FilterViewModel filter )
        {
            var notices = Repository.GetNoticesFilteredCount((FilterModel)filter);
            return notices;
            //return new JsonResult() { Data = notices, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpPost]
        public int GetMyListCount(string Email)
        {
            var notices = Repository.GetMyListCount(Email);
            return notices;
        }
        [HttpGet]
        public ActionResult Show()
        {
            return PartialView();
        }
        [HttpPost]
        [Authorize]
        public ActionResult Create(NoticeViewModel notice) {
            User user = Repository.GetUserByUsername("audfra");
            notice = new NoticeViewModel { Area = 1.9, Buildings = new List<Building>(), CenterLatitude = 18.8, CenterLongitude = 18.8, City = "Kaunas", CreatedOn = DateTime.Now, Description = "desc", Email = "audfra@ktu.lt", Goal = Data.Entities.goals.sale, LandMountainousness = "yes", LandPurpose = "none", Name = "insertTest", NoticeType = "type", Number = "123456789", Price = 99.99, Purpose = Data.Entities.purposes.offer, User = user };
            if (ModelState.IsValid) {
                var noticeModel = (NoticeModel)notice;
                var cr = NoticeService.Create(noticeModel);
            }
            return RedirectToAction("GetNewest", "Notice");
        }

        [HttpPost]
        [Authorize]
        public ActionResult CreateTemp(NoticeViewModel notice)
        {
            User user = Repository.GetUserByEmail(User.Identity.Name); //VEIKIA SU PRISIJUNGIMU
            //User user = Repository.GetUserByUsername("gytis");
            notice = new NoticeViewModel { Area = notice.Area, Buildings = new List<Building>(), CenterLatitude = 18.8, CenterLongitude = 18.8, City = notice.City, CreatedOn = DateTime.Now, Description = notice.Description, Email = notice.Email, Goal = notice.Goal, LandMountainousness = "yes", LandPurpose = "none", Name = notice.Name, NoticeType = notice.NoticeType, Number = notice.Number, Price = notice.Price, Purpose = notice.Purpose, User = user };
            if (ModelState.IsValid)
            {
                var noticeModel = (NoticeModel)notice;
                var cr = NoticeService.Create(noticeModel);
            }
            return Json(new { status = "success" });
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddAddress(AddressViewModel a)
        {
            User user = Repository.GetUserByEmail(User.Identity.Name); //VEIKIA SU PRISIJUNGIMU
            a = new AddressViewModel { Country = a.Country, Region = "Rajonas", City = a.City, Street = a.Street, BuildingNumber = a.BuildingNumber, CreatedOn = DateTime.Now, User = user };
            if (ModelState.IsValid)
            {
                var addressModel = (AddressModel)a;
                var cr = AddressService.Create(addressModel);
            }
            return Json(new { status = "success" });
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddBuilding(BuildingViewModel b)
        {
            User user = Repository.GetUserByEmail(User.Identity.Name); //VEIKIA SU PRISIJUNGIMU
            Notice notice = Repository.GetNoticeById(GetNewestId());
            Address address = Repository.GetAddressById(GetNewestAddressId());
            b = new BuildingViewModel { TotalArea = b.TotalArea, Category = 0, Condition = b.Condition, ConstructionFrom = DateTime.Now, Conveniences = b.Conveniences, ConstructionTo = DateTime.Now, Floors = b.Floors, HasPlumbing = b.HasPlumbing, HasWater = b.HasWater, Heating = b.Heating, Purpose = b.Purpose, Rooms = b.Rooms, Type = b.Type, Layout = null, Notice = notice, Address = address };
            //b = new BuildingViewModel { TotalArea = b.TotalArea, Category = 0, Condition = b.Condition, ConstructionFrom = DateTime.Now, Conveniences = b.Conveniences, ConstructionTo = DateTime.Now, Floors = b.Floors, HasPlumbing = true, HasWater = true, Heating = b.Heating, Purpose = b.Purpose, Rooms = b.Rooms, Type = b.Type, Layout = "betkas", Notice = notice, Address = address };
            //if (ModelState.IsValid)
            //{
                var buildingModel = (BuildingModel)b;
                var cr = BuildingService.Create(buildingModel);
            //}
            return Json(new { status = "success" });
        }

        //[HttpPost]
        //[Authorize]
        public ActionResult Delete(int id) {
            NoticeService.Delete(id);
            return RedirectToAction("GetNewest", "Notice");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Edit(NoticeViewModel notice) {
            User user = Repository.GetUserByUsername("audfra");
            notice = new NoticeViewModel { Area = 2.1, Buildings = new List<Building>() {Repository.GetBuilding(1), Repository.GetBuilding(2) }, CenterLatitude = 19.9, CenterLongitude = 19.9, City = "Vilnius", CreatedOn = DateTime.Now, Description = "edited", Email = "audfra@ktu.lt", Goal = Data.Entities.goals.sale, LandMountainousness = "yes", LandPurpose = "none", Name = "editTest", NoticeType = "type", Number = "123456789", Price = 9999.99, Purpose = Data.Entities.purposes.offer, User = user, Id = 8 };
            if (ModelState.IsValid) {
                var noticeModel = (NoticeModel)notice;
                var ed = NoticeService.Edit(noticeModel);
            }
            return RedirectToAction("GetNewest", "Notice");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        [Authorize]
        public ActionResult Upload()
        {
            return PartialView();
        }
    }
}