
using NTportal.BusinessLogic.Interfaces;
using NTportal.Data.DatabaseContext;
using NTportal.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NTportal.Controllers
{
    public class HomeController : Controller
    {

        public static MySqlDbContext db;

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult MainPage()
        {
            return PartialView();
        }

        public ActionResult Tab1()
        {
            return PartialView();
        }
        public ActionResult Tab2()
        {
            return PartialView();
        }

        

    }
}