﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

using NTportal.BusinessLogic;
using NTportal.BusinessLogic.Interfaces;
using NTportal.Data.DatabaseContext;
using NTportal.Data.Entities;
using NTportal.Data.Models;
using NTportal.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NTportal.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService UserService;
        private readonly IRepository Repository;
        public UserController(IUserService us, IRepository rp)
        {
            UserService = us;
            Repository = rp;
        }
        //[ActionName("Login")]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult LoginLogic(LoginViewModel user)
        {
            //System.Web.HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            if (ModelState.IsValid)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    var userClaim = UserService.Authorise((LoginModel)user);
                    if (userClaim != null)
                    {
                        HttpContext.GetOwinContext().Authentication.SignIn(
                        new AuthenticationProperties { IsPersistent = false }, userClaim);
                        var claims=userClaim.Claims.ToArray();
                        if (claims[3].Value !="Admin")
                        {
                            return Json(new { status = "success", message = "Login successful",role="user" });
                        }
                        else
                        {
                            return Json(new { status = "success", message = "Login successful", role = "admin" });
                        }                
                    }
                    else
                    {
                        return Json(new { status = "error", message = "noMatch" });
                    }
                }
                //lo
                return Json(new { status = "error", message = "noMatch" });
            }

            return Json(new { status = "error", message = "noMatch" });
        }
        [HttpPost]
        //[Authorize]
        [AllowAnonymous]
        public JsonResult AutoFill()
        {
            if (!User.Identity.IsAuthenticated)
            {
            }
            string a=User.Identity.Name;
            string b = User.Identity.AuthenticationType;
            if (!User.Identity.IsAuthenticated)
            {
                return Json(new { status = "logOut", role = "" });
            }
            else
            {
                var claims = ((ClaimsIdentity)User.Identity).Claims.ToArray();

                if (claims[3].Value.ToLower()!="admin")
                {
                    return Json(new { status = "logged", role = "user" ,name= User.Identity.Name });
                }
                else
                {
                    return Json(new { status = "logged", role = "admin", name= User.Identity.Name });
                }
            }
        }
        public JsonResult EditUser(AdressChangeViewModel info)
        {
            Repository.AditionalUser((AddressChangeModel)info);
            return Json(new { status = "logged", role = "admin", name = User.Identity.Name });
        }
        public JsonResult GetUserInfo(int? id)
        {
            
            JsonResult userInfo;
            if (id != null)
            {
                userInfo = Repository.GetUserInfo((int)id);
            }else {
                try
                {
                    User user = Repository.GetUserByEmail(User.Identity.Name);
                    userInfo = Repository.GetUserInfo(user.id);
                }
                catch
                {
                    return Json(new { status = "error" });
                }
                
            }
            return userInfo;
        }
        public int GetUserCount(string name)
        {
            var count = Repository.GetUserCount(name);
            return count;
        }
        public JsonResult GetUsers(string name,string role, int page)
        {
            page = page - 1;
            if (page < 0)
            {
                page = 0;
            }
            var users = Repository.GetUsers(name,role , page);
            return users;
        }


        public ActionResult Login()
        {
            return PartialView();
        }
        [Authorize(Roles = "Admin, Consumer")]
        public ActionResult ProfileMain()
        {
            return PartialView();
        }
        [Authorize(Roles = "Admin, Consumer")]
        public ActionResult ProfileInfo()
        {
            return PartialView();
        }
        [Authorize(Roles = "Admin, Consumer")]
        public ActionResult ProfileEdit()
        {
            return PartialView();
        }
        [Authorize(Roles ="Admin")]
        public ActionResult AdminMain()
        {
            return PartialView();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult UserList()
        {
            return PartialView();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult AdminUserInfo()
        {
            return PartialView();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult AdminUserEdit()
        {
            return PartialView();
        }
        [Authorize(Roles = "Admin")]
        public ActionResult UserNotices()
        {
            return PartialView();
        }
        /* [HttpPost]
         [AllowAnonymous]
         public JsonResult Register(RegisterViewModel user)
         {
             if (ModelState.IsValid)
             {
                 if (!User.Identity.IsAuthenticated)
                 {
                     var usrModel = (UserModel)user;
                     usrModel.IP = Request.UserHostAddress;

                     var regRsp = UserService.Register(usrModel);
                     var result = new
                     {
                         Error = regRsp,
                         UserInfo = new
                         {
                             Email = usrModel.Email,
                             Password = usrModel.Password
                         }

                     };
                     if (String.IsNullOrWhiteSpace(regRsp))
                     {
                         RedirectToAction("LoginLogic", new { Email = usrModel.Email, Password = usrModel.Password });
                         return new JsonResult() { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                         //return RedirectToAction("LoginLogic", new { Email = usrModel.Email, Password = usrModel.Password });
                     }

                     return new JsonResult() { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

                 }

                 //return View("You are logged in.");
             }
             var result2 = new
             {
                 Error = 1,

             };

             return new JsonResult() { Data = result2, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
         }*/
        [HttpPost]
         public ActionResult RegisterLogic(RegisterViewModel user)
         {
            if (ModelState.IsValid)
             {
                 if (!User.Identity.IsAuthenticated)
                 {
                     var usrModel = (UserModel)user;
                     usrModel.IP = Request.UserHostAddress;

                     var regRsp = UserService.Register(usrModel);
                     if (String.IsNullOrWhiteSpace(regRsp))
                     {
                        LoginViewModel usr = new LoginViewModel();
                        usr.Email = usrModel.Email;
                        usr.Password = usrModel.Password;
                        //LoginLogic(usr);
                        return Json(new { status = "error", message = "0" });
                    }
                    return Json(new { status = "error", message = regRsp });

                }

                 return View("You are logged in.");
             }


             return RedirectToAction("Index", "Home");
         }
        public ActionResult Register()
        {  
            return PartialView();
        }

        [Authorize]
        [HttpPost]
        public ActionResult LogOff()
        {
            
            System.Web.HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return Json(new { status = "success", message = "0" });
        }

        [Authorize]
        [HttpGet]
        public virtual async Task<List<NoticeViewModel>> GetNoticesAsync(bool isASC, int count)
        {
                return (await Repository.GetNoticesAsync(isASC, count)).Cast<NoticeViewModel>().ToList();
        }


        }
}