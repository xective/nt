﻿using NTportal.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NTportal.Models.ViewModels
{
    public class RegisterViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string IP { get; set; }

        //public string PasswordConfirmation { get; set; }
        public string PhoneNumber { get; set; }
        public string City { get; set; }

        public static explicit operator UserModel(RegisterViewModel v)
        {
            return new UserModel
            {
                Email = v.Email,
                Password = v.Password,
                UserName = v.UserName,
                PhoneNumber = v.PhoneNumber,
                City = v.City
                
            };
        }
    }
}