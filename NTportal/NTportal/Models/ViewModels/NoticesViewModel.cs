﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NTportal.Models.ViewModels
{
    public class NoticesViewModel
    {
        public List<NoticeViewModel> Notices { get; set; }
    }
}