﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTportal.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace NTportal.Models.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        /*[MaxLength(30)]
        [MinLength(6)]*/
        public string Email { get; set; }
        [Required]
        /*[MaxLength(20)]
        [MinLength(6)]*/
        public string Password { get; set; }

        public static explicit operator LoginViewModel(UserModel v)
        {
            return new LoginViewModel
            {
                Email = v.Email,
                Password = v.Password
            };
        }
        public static explicit operator LoginModel(LoginViewModel v)
        {
            return new LoginModel
            {
                Email = v.Email,
                Password = v.Password
            };
        }

    }
}