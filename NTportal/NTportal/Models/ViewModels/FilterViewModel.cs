﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTportal.Data.Models;

namespace NTportal.Models.ViewModels
{
    public class FilterViewModel
    {
        public string Name { get; set; }
        public string City { get; set; }
        public string Type { get; set; }

        public int Goal { get; set; }
        public int RoomFrom { get; set; }
        public int RoomTo { get; set; }
        public double PriceFrom { get; set; }
        public double PriceTo { get; set; }
        public double FlatSizeFrom { get; set; }
        public double FlatSizeTo { get; set; }
        public double AreaFrom { get; set; }
        public double AreaTo { get; set; }
        public int FloorFrom { get; set; }
        public int FloorTo { get; set; }


        public static explicit operator FilterModel(FilterViewModel v)
        {
            return new FilterModel
            {
                Name = v.Name,
                City=v.City,
                RoomCountFrom=v.RoomFrom,
                RoomCountTo=v.RoomTo,
                PriceFrom=v.PriceFrom,
                PriceTo=v.PriceTo,
                FlatSizeFrom=v.FlatSizeFrom,
                FlatSizeTo=v.FlatSizeTo,
                SizeFrom=v.AreaFrom,
                SizeTo=v.AreaTo,
                FloorFrom=v.FloorFrom,
                FloorTo=v.FloorTo,
                Type=v.Type,
                Goal=v.Goal
            };
        }
    }
}