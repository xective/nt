﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using NTportal.Data.Models;
using NTportal.Data.Entities;

namespace NTportal.Models.ViewModels
{
    public class NoticeViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public User User { get; set; }
        public string NoticeType { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Description { get; set;}
        public goals Goal { get; set; }
        public purposes Purpose { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public double? Price { get; set; }
        public double Area { get; set; }
        public string LandMountainousness { get; set; }
        public string LandPurpose { get; set; }
        public double CenterLongitude { get; set; }
        public double CenterLatitude { get; set; }
        public virtual ICollection<Building> Buildings { get; set; }

        public static explicit operator NoticeModel(NoticeViewModel n) {
            return new NoticeModel {
                Area = n.Area,
                Buildings = n.Buildings,
                CenterLatitude = n.CenterLatitude,
                CenterLongitude = n.CenterLongitude,
                City = n.City,
                CreatedOn = n.CreatedOn,
                Description = n.Description,
                Email = n.Email,
                Goal = n.Goal,
                Id = n.Id,
                LandMountainousness = n.LandMountainousness,
                LandPurpose = n.LandPurpose,
                Name = n.Name,
                NoticeType = n.NoticeType,
                Number = n.Number,
                Price = n.Price,
                Purpose = n.Purpose,
                User = n.User
            };
        }
        public static explicit operator NoticeViewModel(NoticeModel n)
        {
            return new NoticeViewModel
            {
                Area = n.Area,
                Buildings = n.Buildings,
                CenterLatitude = n.CenterLatitude,
                CenterLongitude = n.CenterLongitude,
                City = n.City,
                CreatedOn = n.CreatedOn,
                Description = n.Description,
                Email = n.Email,
                Goal = n.Goal,
                Id = n.Id,
                LandMountainousness = n.LandMountainousness,
                LandPurpose = n.LandPurpose,
                Name = n.Name,
                NoticeType = n.NoticeType,
                Number = n.Number,
                Price = n.Price,
                Purpose = n.Purpose,
                User = n.User
            };
        }

    }
}