﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTportal.Data.Models;

namespace NTportal.Models.ViewModels
{
    public class AdressChangeViewModel
    {
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string Email { get; set; }
        public int AddressId { get; set; }
        public string Role { get; set; }
        public static explicit operator AddressChangeModel(AdressChangeViewModel v)
        {
            return new AddressChangeModel
            {
                Email = v.Email,
                City = v.City,
                Country=v.Country,
                Street=v.Street,
                StreetNo=v.StreetNo,
                AddressId=v.AddressId,
                Role=v.Role
            };
        }
    }
}