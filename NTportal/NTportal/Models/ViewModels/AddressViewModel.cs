﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTportal.Data.Models;
using NTportal.Data.Entities;

namespace NTportal.Models.ViewModels
{
    public class AddressViewModel
    {
        public int Id { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string BuildingNumber { get; set; }
        public DateTime CreatedOn { get; set; }
        public User User { get; set; }

        public static explicit operator AddressModel(AddressViewModel a)
        {
            return new AddressModel
            {
                Id = a.Id,
                Country = a.Country,
                Region = a.Region,
                City = a.City,
                Street = a.Street,
                BuildingNumber = a.BuildingNumber,
                CreatedOn = a.CreatedOn,
                User = a.User
            };
        }

        public static explicit operator AddressViewModel(AddressModel a)
        {
            return new AddressViewModel
            {
                Id = a.Id,
                Country = a.Country,
                Region = a.Region,
                City = a.City,
                Street = a.Street,
                BuildingNumber = a.BuildingNumber,
                CreatedOn = a.CreatedOn,
                User = a.User
            };
        }
    }
}
