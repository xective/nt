﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NTportal.Data.Entities;
using NTportal.Data.Models;

namespace NTportal.Models.ViewModels
{
    public class BuildingViewModel
    {
        public int BuildingID { get; set; }
        public virtual Address Address { get; set; }
        public virtual Notice Notice { get; set; }
        public DateTime ConstructionFrom { get; set; }
        public DateTime ConstructionTo { get; set; }
        public double TotalArea { get; set; }
        public int Floors { get; set; }
        public int Rooms { get; set; }
        public string Conveniences { get; set; }
        public string Layout { get; set; }
        public string Purpose { get; set; }
        public bool HasWater { get; set; }
        public bool HasPlumbing { get; set; }
        public conditions Condition { get; set; }
        public categories Category { get; set; }
        public types Type { get; set; }
        public heatingType Heating { get; set; }

        public static explicit operator BuildingModel(BuildingViewModel b)
        {
            return new BuildingModel
            {
                Address = b.Address,
                BuildingID = b.BuildingID,
                Category = b.Category,
                Condition = b.Condition,
                ConstructionFrom = b.ConstructionFrom,
                ConstructionTo = b.ConstructionTo,
                Conveniences = b.Conveniences,
                Floors = b.Floors,
                HasPlumbing = b.HasPlumbing,
                HasWater = b.HasWater,
                Heating = b.Heating,
                Layout = b.Layout,
                Notice = b.Notice,
                Purpose = b.Purpose,
                Rooms = b.Rooms,
                TotalArea = b.TotalArea,
                Type = b.Type
            };
        }

        public static explicit operator BuildingViewModel(BuildingModel b)
        {
            return new BuildingViewModel
            {
                Address = b.Address,
                BuildingID = b.BuildingID,
                Category = b.Category,
                Condition = b.Condition,
                ConstructionFrom = b.ConstructionFrom,
                ConstructionTo = b.ConstructionTo,
                Conveniences = b.Conveniences,
                Floors = b.Floors,
                HasPlumbing = b.HasPlumbing,
                HasWater = b.HasWater,
                Heating = b.Heating,
                Layout = b.Layout,
                Notice = b.Notice,
                Purpose = b.Purpose,
                Rooms = b.Rooms,
                TotalArea = b.TotalArea,
                Type = b.Type
            };
        }
    }
}