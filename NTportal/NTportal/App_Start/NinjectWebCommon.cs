[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NTportal.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NTportal.App_Start.NinjectWebCommon), "Stop")]

namespace NTportal.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using BusinessLogic.Interfaces;
    using BusinessLogic.Implementation;
    using BusinessLogic;
    using Data.DatabaseContext;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                //Bindings
                kernel.Bind<IUserService>().To<UserService>();
                kernel.Bind<IRepository>().To<Repository>().InRequestScope();
                kernel.Bind<INoticeService>().To<NoticeService>();
                kernel.Bind<IAddressService>().To<AddressService>();
                kernel.Bind<IBuildingService>().To<BuildingService>();
                kernel.Bind<ILoggerService>().To<LoggerService>();
                kernel.Bind<MySqlDbContext>().ToSelf();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
        }        
    }
}
