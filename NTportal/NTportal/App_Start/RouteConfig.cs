﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NTportal
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "home",
                url: "home",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "login",
                url: "login",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "register",
                url: "register",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "search",
                url: "search",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "newest",
                url: "newest",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "profile",
                url: "profile",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "profileinfo",
                url: "profile/info",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "profileedit",
                url: "profile/edit",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "profilenotices",
                url: "profile/myNotices",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "upload",
                url: "upload",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "admin",
                url: "admin",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "usersList",
                url: "admin/users",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "userinfoAdmin",
                url: "admin/userinfo/{id:int}",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "userEditAdmin",
                url: "admin/useredit/{id:int}",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "noticeShow",
                url: "notice/showinfo/{id:int}",
                defaults: new { controller = "Home", action = "Index" });
            routes.MapRoute(
                name: "UsersNotices",
                url: "admin/usernotices/{id:int}",
                defaults: new { controller = "Home", action = "Index" });

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters*
                new{controller = "Home",
                action = "Index",
                id = UrlParameter.Optional
                });
            /* routes.MapRoute(
                name: "Default2",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });*/
          /*  routes.MapRoute(
                name: "login",
                url: "user/Login",
                defaults: new { controller = "User", action = "Login" });*/

            /*routes.MapRoute(
                name: "register",
                url: "user/Register",
                defaults: new { controller = "User", action = "Register" });*/
           /* routes.MapRoute(
                name: "home",
                url: "home/MainPage",
                defaults: new { controller = "Home", action = "MainPage" });*/
           /* routes.MapRoute(
                name: "notice",
                url: "notice/GetNewest",
                defaults: new { controller = "Notice", action = "GetNewest" });*/
          /*  routes.MapRoute(
                name: "getnotice",
                url: "notice/GetNotice",
                defaults: new { controller = "Notice", action = "GetNotice" });
            routes.MapRoute(
                name: "filtered",
                url: "notice/GetList",
                defaults: new { controller = "Notice", action = "GetList" });
            routes.MapRoute(
                name: "loginLogic",
                url: "user/LoginLogic",
                defaults: new { controller = "User", action = "LoginLogic" });
            routes.MapRoute(
                name: "testpage",
                url: "home/TestPage",
                defaults: new { controller = "Home", action = "TestPage" });
            routes.MapRoute(
                name: "tab1",
                url: "home/tab1",
                defaults: new { controller = "Home", action = "Tab1" });
            routes.MapRoute(
                name: "tab2",
                url: "home/tab2",
                defaults: new { controller = "Home", action = "Tab2" });
            routes.MapRoute(
                name: "userInfo",
                url: "user/getuserinfo",
                defaults: new { controller = "User", action = "GetUserInfo" });

            /*routes.MapRoute(
                name: "tab1",
                url: "home/search",
                defaults: new { controller = "Home", action = "Tab1" });
            routes.MapRoute(
                name: "tab2",
                url: "home/newest",
                defaults: new { controller = "Home", action = "Tab2" });*/
            /* routes.MapRoute(
                  name: "Default",
                  url: "{*url}",
                  defaults: new { controller = "Home", action = "Index" });*/
           



        }
    }
}
