﻿var UploadController = function ($scope, $http, $window) {

    angular.module('inputErrorsApp', ['ngMaterial', 'ngMessages'])
    console.log('controller started');
    $scope.objectType = '';
    $scope.types = ('Butas  Namas').split('  ').map(function (type) { return { name: type }; });

    $scope.countryType = '';
    $scope.countryTypes = ('Lietuva  Latvija  Estija').split('  ').map(function (cType) { return { name: cType }; });

    $scope.floorType = '';
    $scope.floorTypes = ('1  2  3  4  5  6  7  8  9').split('  ').map(function (fType) { return { name: fType }; });

    $scope.roomType = '';
    $scope.roomTypes = ('1  2  3  4  5  6  7  8  9').split('  ').map(function (rType) { return { name: rType }; });

    $scope.housePurpose = '';
    $scope.housePurposes = ('Gyvenamasis  Komercinis').split('  ').map(function (hPurpose) { return { name: hPurpose }; });

    $scope.heating = '';
    $scope.heatingList = [
    { id: 1, name: 'Dujinis' },
    { id: 2, name: 'Centrinis' },
    { id: 3, name: 'Centrinis kolektorinis' },
    { id: 4, name: 'Elektra' },
    { id: 5, name: 'Mišrus' },
    { id: 6, name: 'Geoterminis' },
    { id: 8, name: 'Krosnimi' },
    { id: 9, name: 'Nešildoma' },
    { id: 10, name: 'Kita' }
    ];

    $scope.bType = '';
    $scope.bTypes = [
   // { id: 0, name: 'Rastinis' },
    { id: 1, name: 'Plytinis' },
    { id: 2, name: 'Karkasinis' },
    { id: 3, name: 'Monolitinis' },
    { id: 4, name: 'Blokinis' },
    { id: 5, name: 'Medinis' },
    { id: 6, name: 'Kita' }
    ];

    $scope.bType = '';
    $scope.bTypes = [
   // { id: 0, name: 'Rastinis' },
    { id: 1, name: 'Plytinis' },
    { id: 2, name: 'Karkasinis' },
    { id: 3, name: 'Monolitinis' },
    { id: 4, name: 'Blokinis' },
    { id: 5, name: 'Medinis' },
    { id: 6, name: 'Kita' }
    ];

    $scope.conditionType = '';
    $scope.conditionTypes = [
   // { id: 0, name: 'Rastinis' },
    { id: 1, name: 'Turi baldų' },
    { id: 2, name: 'Statomas/remontuojamas' },
    { id: 3, name: 'Reikia remonto' },
    ];

    $scope.showNoticeForm = true;
    $scope.showAddressForm = false;
    $scope.showBuildingForm = false;

    $scope.backToNotice = function() {
        $scope.showNoticeForm = true;
        $scope.showAddressForm = false;
        $scope.showBuildingForm = false;
        console.log("showNoticeForm", $scope.showNoticeForm);
        console.log("showAddressForm", $scope.showAdressForm);
    }

    $scope.backToAddress = function () {
        $scope.showNoticeForm = false;
        $scope.showBuildingForm = false;
        $scope.showAddressForm = true;
        console.log("showNoticeForm", $scope.showNoticeForm);
        console.log("showAddressForm", $scope.showAdressForm);
    }

    $scope.submit = function () {
        $scope.showNoticeForm = false;
        $scope.showBuildingForm = false;
        $scope.showAddressForm = true;
        console.log("Skelbimo tipas ", $scope.objectType);
        console.log("Skelbimo tikslas ", $scope.goal);
        console.log("Skelbimo paskirtis ", $scope.purpose);
        console.log("Pavadinimas ", $scope.name);
        console.log("Aprasymas ", $scope.description);
        console.log("El pastas", $scope.email);
        console.log("Numeris ", $scope.number);
        console.log("Kaina", $scope.price);
        console.log("Miestas", $scope.city);
        //$http.post(
        //    '/Notice/CreateTemp', {
        //        Email: $scope.email,
        //        NoticeType: $scope.objectType,
        //        Purpose: $scope.purpose,
        //        Goal: $scope.goal,
        //        Description: $scope.description,
        //        Name: $scope.name,
        //        Number: $scope.number,
        //        Price: $scope.price,
        //        City: $scope.city

        //    })
    }

    $scope.submitAddress = function () {
        $scope.showNoticeForm = false;
        $scope.showAddressForm = false;
        $scope.showBuildingForm = true;
        console.log("Valstybe ", $scope.countryType);
        console.log("Miestas ", $scope.acity);
        console.log("Gatve ", $scope.street);
        console.log("Namas ", $scope.buildingNumber);

        //$http.post(
        //    '/Notice/AddAddress', {
        //        City: $scope.acity,
        //        Country: $scope.countryType,
        //        Street: $scope.street,
        //        BuildingNumber: $scope.buildingNumber
        //    })
    }

    $scope.submitBuilding = function () {
        console.log("Plotas: ", $scope.area);
        console.log("Aukstas: ", $scope.floorType);
        console.log("Statymo metai: ", $scope.buildyear);
        console.log("Patogumai: ", $scope.features);
        console.log("Paskirtis: ", $scope.housePurpose);
        console.log("Vandentiekis: ", $scope.water);
        console.log("Kanalizacija: ", $scope.sewerage);
        console.log("Sildymas: ", $scope.heating);
        console.log("Kambariai: ", $scope.roomType);
        console.log("Pastato tipas: ", $scope.bType);
        console.log("Bukle: ", $scope.conditionType);
        console.log("Pradedam ikelima")
        $http.post(
            '/Notice/CreateTemp', {
                Area: $scope.area,
                Email: $scope.email,
                NoticeType: $scope.objectType,
                Purpose: $scope.purpose,
                Goal: $scope.goal,
                Description: $scope.description,
                Name: $scope.name,
                Number: $scope.number,
                Price: $scope.price,
                City: $scope.city

            }).success(function () {
                console.log("Success su Create");
                $http.post(
                    '/Notice/AddAddress', {
                        City: $scope.city,
                        Country: $scope.countryType,
                        Street: $scope.street,
                        BuildingNumber: $scope.buildingNumber
                    }).success(function () { 
                        console.log("Success su AddAddress");
                        $http.post(
                            '/Notice/AddBuilding', {
                                TotalArea: $scope.area,
                                Condition: $scope.conditionType,
                                ConstructionFrom: $scope.buildyear,
                                Conveniences: $scope.features,
                                Floors: $scope.floorType,
                                HasPlumbing: $scope.sewerage,
                                HasWater: $scope.water,
                                Heating: $scope.heating,
                                Purpose: $scope.purpose,
                                Rooms: $scope.roomType,
                                Type: $scope.bType
                            }).success(function () {
                                console.log("Success su AddBuilding");
                                $window.location.href = '/';
                            })
                    })
            })


    }
}

// The $inject property of every controller (and pretty much every other type of object in Angular) needs to be a string array equal to the controllers arguments, only as strings
UploadController.$inject = ['$scope', '$http', '$window'];

