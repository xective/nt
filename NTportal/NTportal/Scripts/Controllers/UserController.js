﻿var UserController = function ($scope, $stateParams, UserService,AuthService, $state) {
    UserService.getUserInfo().then(function (response) {
        $scope.loading = false;
        var rsp2 = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
        console.log("Response" + rsp2);
        console.log("createDate "+response.CreationDate)
        var mydate = parseJsonDate(response.CreationDate);
        $scope.userInfo = response;
        $scope.userInfo.CreationDate = mydate;
        var rsp = JSON.stringify($scope.userInfo, null, 4);
        console.log("info " + rsp);
    },
     function (err) {
         $scope.loading = false;
         console.log("UserInfo error");
         $scope.message = err.error_description;
     });
    $scope.save = function () {
        //var rsp = JSON.stringify(AuthService.authentication, null, 4); // (Optional) beautiful indented output.
       // console.log("Response" + AuthService.authentication.userName);
        UserService.editUserInfo($scope.userInfo).then(function (response) {
            $scope.loading = false;
            //$scope.notices = response;
            $state.go('profile.info')
        },
     function (err) {
         $scope.loading = false;
         $scope.message = err.error_description;
     });;
       
    }
    function parseJsonDate(jsonDate) {
        var offset = new Date().getTimezoneOffset() * 60000;
        var parts = /\/Date\((-?\d+)([+-]\d{2})?(\d{2})?.*/.exec(jsonDate);
        if (parts[2] == undefined) parts[2] = 0;
        if (parts[3] == undefined) parts[3] = 0;
        d = new Date(+parts[1] + offset + parts[2] * 3600000 + parts[3] * 60000);
        date = d.getDate();// + 1;
        date = date < 10 ? "0" + date : date;
        mon = d.getMonth() + 1;
        mon = mon < 10 ? "0" + mon : mon;
        year = d.getFullYear();
        return (date + "." + mon + "." + year);
    };
}

// The $inject property of every controller (and pretty much every other type of object in Angular) needs to be a string array equal to the controllers arguments, only as strings
UserController.$inject = ['$scope', '$stateParams', 'UserService','AuthService', '$state'];