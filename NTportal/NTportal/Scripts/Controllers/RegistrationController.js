﻿var RegistrationController = function ($scope, $http, $stateParams, $location, AuthService) {
    authentication = AuthService.authentication;
    if (authentication.isAuth) {
        $location.path('/home');
    }
    var json = 'http://ipv4.myexternalip.com/json';
    var ipAddress;
    $scope.registration = {
        email: "",
        username: "",
        password: "",
        IP: ""
    };
    (function () {
        //Bunch of code...
        $http.get(json)
            .success(function (data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
                // ipAddress = JSON.stringify(data.ip);
                ipAddress = data.ip;
            })
            .error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    })();

    /*var startTimer = function () {
        var timer = $timeout(function () {
            $timeout.cancel(timer);
            $location.path('/login');
        }, 2000);
    }*/
    $scope.submit = function () {
        if (!$scope.registration.email.match('@')) {
            $scope.email_bad = true;
        } else {
            $scope.email_bad = false;
        }
        if ($scope.registration.password.length < 8) {
            $scope.pass_short = true;
        } else {
            $scope.pass_short = false;
        }
        if (!$scope.registration.password.match(/^(?=.*[0-9]).+$/)) {
            $scope.pass_not_number = true;
        } else {
            $scope.pass_not_number = false;
        }
        if ($scope.pass_short && $scope.pass_not_number) {
            $scope.pass_bad = true;
        } else {
            $scope.pass_bad = false;
        }
        if ($scope.registration.password != $scope.reenter_password) {
            $scope.pass_not_match = true;
        } else {
            $scope.pass_not_match = false;
        }
        if (!$scope.email_bad && !$scope.pass_bad && !$scope.pass_not_match) {
            $scope.waiting = true;
            $scope.registration.IP = ipAddress;


            AuthService.saveRegistration($scope.registration).then(function (response) {

                $scope.savedSuccessfully = true;
               /*console.log("success");
                var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
                console.log("Response" + rsp);*/
                if (response.data.status == "error") {
                    switch (response.data.message) {
                        case "1":
                            $scope.emailExists = true;
                            $scope.userExists = false;
                            break;
                        case "2":
                            $scope.userExists = true;
                            $scope.emailExists = false;
                            break;
                        case "3":
                            $scope.emailExists = true;
                            $scope.userExists = true;
                            break;
                        default:
                            $scope.emailExists = false;
                            $scope.userExists = false;
                            //startTimer();
                            $location.path('/login');
                    }
                }
                $scope.message = "User has been registered successfully, you will be redicted to login page in 2 seconds.";
               // startTimer();

            },
         function (response) {
             var errors = [];
             for (var key in response.data.modelState) {
                 for (var i = 0; i < response.data.modelState[key].length; i++) {
                     console.log("err");
                     errors.push(response.data.modelState[key][i]);
                 }
             }
             $scope.message = "Failed to register user due to:" + errors.join(' ');
         });
            /*var result = RegistrationFactory($scope.registration.email, $scope.registration.username, $scope.registration.password, $scope.registration.IP, $scope.userExists, $scope.emailExists);
            console.log("Register: "+result)
            result.then(function (result) {
                if (result.success) {
                    //window.location.pathname = '/Home/Index';
                    $location.path('/home');
                } else {
                    $scope.waiting = false;
                    //$scope.registerForm.registrationFailure = true;
                }
            });*/
        }
    };
}
RegistrationController.$inject = ['$scope', '$http', '$stateParams', '$location', 'AuthService'];