﻿var UsersNoticesController = function ($scope, $stateParams, NoticeService, $state) {
    NoticeService.getUsersNoticesCount($stateParams.id).then(function (response) {
        var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
        $scope.totalItems = parseInt(response, 10);
        $scope.currentPage = parseInt($stateParams.page, 10);
        $scope.maxSize = 5;
        $scope.bigTotalItems = parseInt(rsp, 10);
        $scope.bigCurrentPage = parseInt($stateParams.page, 10);
        console.log("total " + $scope.totalItems);
        var state = JSON.stringify($stateParams, null, 4);
        console.log("state "+state)
    },
    function (err) {
        $scope.message = err.error_description;
    });
    $scope.loading = true;
    NoticeService.getUsersNotices($stateParams).then(function (response) {
        $scope.loading = false;
        var rsp2 = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
        console.log("Response" + rsp2);
        $scope.notices = response;
        if ($scope.notices == 0) {
            $scope.emptyList = true;
        }
        else {
            $scope.emptyList = false;
        }
        /* console.log("createDate " + response.CreationDate)
         var mydate = parseJsonDate(response.CreationDate);
         $scope.userInfo = response;
         $scope.userInfo.CreationDate = mydate;
         var rsp = JSON.stringify($scope.userInfo, null, 4);
         console.log("info " + rsp);*/
    },
     function (err) {
         $scope.loading = false;
         $scope.message = err.error_description;
     });
    this.page = parseInt($stateParams.page, 10);
    var self = this;
    $scope.pageChanged = function () {
        console.log('Page changed to: ' + $scope.currentPage);
        $state.go('.', { page: ($scope.currentPage) }, { notify: false });
    };
    /* $scope.save = function () {
         //var rsp = JSON.stringify(AuthService.authentication, null, 4); // (Optional) beautiful indented output.
         // console.log("Response" + AuthService.authentication.userName);
         UserService.editUserInfo($scope.userInfo).then(function (response) {
             $scope.loading = false;
             //$scope.notices = response;
             $state.go('profile.info')
         },
      function (err) {
          $scope.loading = false;
          $scope.message = err.error_description;
      });;
 
     }*/

}

// The $inject property of every controller (and pretty much every other type of object in Angular) needs to be a string array equal to the controllers arguments, only as strings
UsersNoticesController.$inject = ['$scope', '$stateParams', 'NoticeService', '$state'];