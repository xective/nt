﻿var LoginController = function ($scope, $stateParams, $location, AuthService) {
    authentication = AuthService.authentication;
    if (authentication.isAuth) {
        $location.path('/home');
    }
    $scope.loginData = {
        email: "",
        password: ""
    };

    $scope.submit = function () {
        if (!$scope.loginData.email.match('@')) {
            $scope.email_bad = true;
        } else {
            $scope.email_bad = false;
        }
        if ($scope.loginData.password.length < 8) {
            $scope.pass_short = true;
        } else {
            $scope.pass_short = false;
        }
        if (!$scope.loginData.password.match(/^(?=.*[0-9]).+$/)) {
            $scope.pass_not_number = true;
        } else {
            $scope.pass_not_number = false;
        }
        if ($scope.pass_short && $scope.pass_not_number) {
            $scope.pass_bad = true;
        } else {
            $scope.pass_bad = false;
        }

        if (!$scope.email_bad) {
            $scope.waiting = true;
            AuthService.login($scope.loginData).then(function (response) {
                var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
                console.log("Response auth" + rsp);
                if (response.status=="success") {
                    $scope.waiting = false;
                    $scope.badMatch = false;
                    $location.path('/home');
                } else {
                    $scope.badMatch = true;
                    $scope.waiting = false;
                }
                   
            },
         function (err) {
             console.log("Error" + err);
             $scope.message = err.error_description;
         });
            /*var result = LoginFactory($scope.email,  $scope.password);
            console.log(result)
            result.then(function (result) {
                if (result.success) {
                    /*if ($scope.loginForm.returnUrl !== undefined) {
                        $location.path('/routeOne');
                    } else {
                        $location.path($scope.loginForm.returnUrl);
                    }
                    console.log("ok");
                    console.log(result);
                    $location.path('/home');
                } else {
                    //$scope.loginForm.loginFailure = true;
                }
            });*/
        }
    };
}




/*var LoginController = function ($scope, $routeParams) {
    $scope.loginForm = {
        emailAddress: '',
        password: '',
        rememberMe: false,
        returnUrl: $routeParams.returnUrl
    };

    $scope.login = function () {
        //todo
    }
}*/

LoginController.$inject = ['$scope', '$stateParams', '$location', 'AuthService'];