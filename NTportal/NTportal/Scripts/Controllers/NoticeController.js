﻿var NoticeController = function ($scope, $stateParams, NoticeService, $state) {
    //NoticeService.setFilterData($stateParams);
    $scope.loading = true;
    NoticeService.getNoticeData($stateParams.id).then(function (response) {
        $scope.loading = false;
        $scope.notice = response[0];
        $scope.notice.Buildings.forEach(parseJsonDate);
        str = JSON.stringify($scope.notice, null, 4); // (Optional) beautiful indented output.
        console.log(str); // Logs output to dev tools console.
    },
     function (err) {
         $scope.loading = false;
         $scope.message = err.error_description;
     });
    function parseJsonDate(item) {
        jsonDate = item.BuildingDate;
        var offset = new Date().getTimezoneOffset() * 60000;
        var parts = /\/Date\((-?\d+)([+-]\d{2})?(\d{2})?.*/.exec(jsonDate);
        if (parts[2] == undefined) parts[2] = 0;
        if (parts[3] == undefined) parts[3] = 0;
        d = new Date(+parts[1] + offset + parts[2] * 3600000 + parts[3] * 60000);
        date = d.getDate();// + 1;
        date = date < 10 ? "0" + date : date;
        mon = d.getMonth() + 1;
        mon = mon < 10 ? "0" + mon : mon;
        year = d.getFullYear();
        item.BuildingDate = (date + "." + mon + "." + year);
        //return (date + "." + mon + "." + year);
    };
}

// The $inject property of every controller (and pretty much every other type of object in Angular) needs to be a string array equal to the controllers arguments, only as strings
NoticeController.$inject = ['$scope', '$stateParams', 'NoticeService', '$state'];