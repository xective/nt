﻿var LandingPageController = function ($scope, $location, $timeout, $mdSidenav, $log, $state, AuthService, SearchService) {

    
        

    $scope.authentication = AuthService.authentication;
    $scope.toggleLeft = buildToggler('left');
    $scope.logOut = function () {
        AuthService.logOut();
        $location.path('/home');
    }
    $scope.filter = SearchService.filter;
    $scope.search = function () {
        //var str = JSON.stringify($scope.filter, null, 4); // (Optional) beautiful indented output.
        //console.log(str);
        $state.go('search', {
            priceFrom: $scope.filter.priceFrom,
            priceTo: $scope.filter.priceTo,
            areaFrom: $scope.filter.areaFrom,
            areaTo: $scope.filter.areaTo,
            city: $scope.filter.city,
            type: $scope.filter.type,
            roomFrom: $scope.filter.roomFrom,
            roomTo: $scope.filter.roomTo,
            floorFrom: $scope.filter.floorFrom,
            floorTo: $scope.filter.floorTo,
            type: $scope.filter.type,
            goal: $scope.filter.goal
        });
    }

    $scope.close = function () {
      // Component lookup should always be available since we are not using `ng-if`
      $mdSidenav('left').close()
        .then(function () {
          $log.debug("close LEFT is done");
        });

    };
    function debounce(func, wait, context) {
        var timer;

        return function debounced() {
            var context = $scope,
                args = Array.prototype.slice.call(arguments);
            $timeout.cancel(timer);
            timer = $timeout(function () {
                timer = undefined;
                func.apply(context, args);
            }, wait || 10);
        };
    }
    function buildToggler(navID) {
        return function () {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav(navID)
              .toggle()
              .then(function () {
                  $log.debug("toggle " + navID + " is done");
              });
        };
    }

    $scope.wantedCity = [
        "Vilnius",
        "Kaunas",
        "Klaipėda",
        "Jonava",
        "Ukmergė",
        "Šiauliai",
        "Panevėžys"
    ];
    $scope.goals = [
    { id: 1, name: 'Nuoma' },
    { id: 2, name: 'Pirkimas' },
    { id: 3, name: 'Mainai' }
    ];
    $scope.types = ('Butas Namas').split(' ').map(function (type) { return { wantedType: type }; });
    $scope.rooms = ('1 2 3 4 5 6 7 8 9').split(' ').map(function (room) { return { wantedRoom: room }; });
    $scope.floors = ('1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18').split(' ').map(function (floor) { return { wantedFloor: floor }; });
    
}

// The $inject property of every controller (and pretty much every other type of object in Angular) needs to be a string array equal to the controllers arguments, only as strings
LandingPageController.$inject = ['$scope', '$location', '$timeout', '$mdSidenav', '$log','$state', 'AuthService','SearchService'];