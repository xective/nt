﻿var SearchController = function ($scope, $stateParams, SearchService, $state) {
    SearchService.setFilterData($stateParams);

    SearchService.getCount($stateParams).then(function (response) {
        var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
        $scope.totalItems = parseInt(response, 10);
        $scope.currentPage = parseInt($stateParams.page, 10);
        $scope.maxSize = 5;
        $scope.bigTotalItems = parseInt(rsp, 10);
        $scope.bigCurrentPage = parseInt($stateParams.page, 10);
        console.log("total " +$scope.totalItems);
    },
    function (err) {
        $scope.message = err.error_description;
    });
    $scope.pageChanged = function () {
        console.log('Page changed to: ' + $scope.currentPage);
        $state.go('.', { page: ($scope.currentPage) }, { notify: false });
    };

    $scope.loading = true;
    SearchService.getNotices($stateParams).then(function (response) {
        $scope.loading = false;
        $scope.notices = response;
        if ($scope.notices == 0) {
            $scope.emptyList = true;
        }
        else {
            $scope.emptyList = false;
        }
    },
     function (err) {
        $scope.loading = false;
        $scope.message = err.error_description;
     });    

}

// The $inject property of every controller (and pretty much every other type of object in Angular) needs to be a string array equal to the controllers arguments, only as strings
SearchController.$inject = ['$scope', '$stateParams', 'SearchService','$state'];