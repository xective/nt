﻿'use strict';
var NoticeService = function ($http, $q) {
    var serviceBase = 'http://localhost:2016/';
    var noticeServiceFactory = {};
   /* var _noticeData = {
        Type: "",
        Price: 0,
        Name: "",
        Description: "",
        Phone: "",
        Email: "",
        Goal: 0,
        Purpose: 0,
        Area:0,
        City:"",
        Building:{
            BuildingDate: "",
            Area: 0,
            RoomCount:0
        }
    }*/
    var _getNoticeData = function (params) {
        var deferred = $q.defer();
        var postData = {
            id: params
        };
        $http.post(serviceBase + 'notice/GetNotice', postData).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get Notice info Error")
            deferred.reject(err);
        });
        return deferred.promise;
    };

    var _getUsersNoticesCount = function (params) {
        var deferred = $q.defer();
        var postData = {
            id: params
        }
        //var rsp = JSON.stringify(postData, null, 4); // (Optional) beautiful indented output.
        //console.log("dataaa" + rsp);
        $http.post(serviceBase + 'notice/GetUsersListCount', postData).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get Notice Error")
            deferred.reject(err);
        });
        return deferred.promise;
    };
    var _getUsersNotices = function (stateData) {
        var deferred = $q.defer();
        var rsp = JSON.stringify(stateData, null, 4); // (Optional) beautiful indented output.
        console.log("Response" + rsp);
        var postData = {
            id: stateData.id,
            Page: stateData.page
        }
        $http.post(serviceBase + 'notice/GetUsersList', postData).success(function (response) {
            var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
            console.log("Response" + rsp);
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get Notice Error")
            deferred.reject(err);
        });
        return deferred.promise;
    };

    /*var _setFilterData = function (data) {
        _filter.priceFrom = data.priceFrom,
        _filter.priceTo = data.priceTo,
        _filter.areaFrom = data.areaFrom,
        _filter.areaTo = data.areaTo,
        _filter.city = data.city
    }*/
    //noticeServiceFactory.noticeData = _noticeData;
    noticeServiceFactory.getNoticeData = _getNoticeData;
    noticeServiceFactory.getUsersNoticesCount = _getUsersNoticesCount
    noticeServiceFactory.getUsersNotices = _getUsersNotices
    return noticeServiceFactory;
}
NoticeService.$inject = ['$http', '$q'];