﻿'use strict';
var UserService = function ($http, $q) {
    var serviceBase = 'http://localhost:2016/';
    var userServiceFactory = {};

    var _params = {
        page: 1,
        name: "",
        role:""

    }
    var _setParams = function (data) {
        _params.name=data.name,
        _params.role=data.role
    }
    var _getUserCount = function (data) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'user/GetUserCount', data).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get User Count Error")
            deferred.reject(err);
        });
        return deferred.promise;
    };

    var _getUsers = function (data) {
        var deferred = $q.defer();
        var rsp = JSON.stringify(data, null, 4); // (Optional) beautiful indented output.
        console.log("Response" + rsp);
        var postData = {
            name: data.name,
            page: data.page,
            role:data.role
        }
        $http.post(serviceBase + 'user/GetUsers', postData).success(function (response) {
            var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
            console.log("Response" + rsp);
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get Notice Error")
            deferred.reject(err);
        });
        return deferred.promise;
    }

    var _getUserInfo = function (param) {
        var deferred = $q.defer();
        console.log("ide " + param);
        var data={
            id:param
        }
        $http.post(serviceBase + '/user/GetUserInfo', data).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get UserData info Error")
            deferred.reject(err);
        });
        return deferred.promise;
    };


    /*var _getUserInfo = function () {
        var deferred = $q.defer();
        $http.post(serviceBase + '/user/GetUserInfo').success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get UserData info Error")
            deferred.reject(err);
        });
        return deferred.promise;
    };*/
    var _getCount = function (data) {
        var deferred = $q.defer();
        var postData = {
            Email: data
        }
        var rsp = JSON.stringify(postData, null, 4); // (Optional) beautiful indented output.
        console.log("dataaa" + rsp);
        $http.post(serviceBase + 'notice/GetMyListCount', postData).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get Notice Error")
            deferred.reject(err);
        });
        return deferred.promise;
    };
    var _getMyNotices = function (data,stateData) {
        var deferred = $q.defer();
        var rsp = JSON.stringify(stateData, null, 4); // (Optional) beautiful indented output.
        console.log("Response" + rsp);
        var postData = {
            Email: data,
            Page:stateData.page
        }
        $http.post(serviceBase + 'notice/GetMyList', postData).success(function (response) {
            var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
            console.log("Response" + rsp);
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get Notice Error")
            deferred.reject(err);
        });
        return deferred.promise;
    };

    var _editUserInfo = function (data) {
        var rsp = JSON.stringify(data, null, 4); // (Optional) beautiful indented output.
        console.log("Response" + rsp);
        var postData = {
            City: data.AditionalInfo.City,
            Country: data.AditionalInfo.Country,
            Street: data.AditionalInfo.Street,
            StreetNo: data.AditionalInfo.StreetNo,
            Email: data.Email,
            AddressId: data.AditionalInfo.AddressId,
            Role:data.Role

        }
        var rsp2 = JSON.stringify(postData, null, 4); // (Optional) beautiful indented output.
        console.log("Post" + rsp2);
        var deferred = $q.defer();
        $http.post(serviceBase + 'user/EditUser',postData).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get Notice Error")
            deferred.reject(err);
        });
        return deferred.promise;
    };
    userServiceFactory.getMyNotices = _getMyNotices;
    userServiceFactory.editUserInfo = _editUserInfo;
    userServiceFactory.getUserInfo = _getUserInfo;
    userServiceFactory.getCount = _getCount;
    userServiceFactory.getUserCount = _getUserCount;
    userServiceFactory.setParams = _setParams;
    userServiceFactory.params = _params;
    userServiceFactory.getUsers=_getUsers
    return userServiceFactory;
}
UserService.$inject = ['$http', '$q'];