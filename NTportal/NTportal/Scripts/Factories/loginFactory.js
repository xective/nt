﻿var LoginFactory=function ($http, $q, $rootScope) {
    return function (email, password) {
        var deferredObject = $q.defer();
       /* $http.get('/User/LoginLogic', {params :{
            Email: email,
            Password: password
            }*/
        $http.post(
            '/User/LoginLogic', {
                Email: email,
                Password: password

            }).
        success(function (data) {
            console.log(data.message);
            /*console.log("Error " + $rootScope.error + "-");
            console.log("Errorinis " + $rootScope.erorinis + "-");
            console.log("type: " + typeof (data.Error));
            console.log("data: " + data.Error);
            str = JSON.stringify(data.UserInfo);
            str = JSON.stringify(data.UserInfo, null, 4); // (Optional) beautiful indented output.
            console.log(str);
            console.log("email " + data.UserInfo.Email);
            console.log("pass " + data.UserInfo.Password);*/
            switch (data.message) {
                case "noMatch":
                    $rootScope.badMatch = true;
                    deferredObject.resolve({ success: false });
                    break;
                default:
                    $rootScope.badMatch = false;
                    deferredObject.resolve({ success: true });
            }
            deferredObject.resolve({ success: true });
        }).error(function () {
            deferredObject.resolve({ success: false });
        });
        return deferredObject.promise;
    }
};
LoginFactory.$inject = ['$http', '$q','$rootScope'];
