﻿var RegistrationFactory=function ($http, $q, $rootScope) {
    return function (email, username, password, ip, userExists, emailExists) {
        var deferredObject = $q.defer();
        $http.post(
            '/User/Register', {
                Email: email,
                UserName: username,
                Password: password,
                IP: ip
            }
            ).
        success(function (data) {
            console.log(data);
            /*console.log("Error " + $rootScope.error + "-");
            console.log("Errorinis " + $rootScope.erorinis + "-");
            console.log("type: " + typeof (data.Error));
            console.log("data: " + data.Error);
            str = JSON.stringify(data.UserInfo);
            str = JSON.stringify(data.UserInfo, null, 4); // (Optional) beautiful indented output.
            console.log(str);
            console.log("email " + data.UserInfo.Email);
            console.log("pass " + data.UserInfo.Password);*/
            switch (data.message) {
                case "1":
                    $rootScope.emailExists = true;
                    $rootScope.userExists = false;
                    console.log("data: " + data.Error);
                    deferredObject.resolve({ success: false });
                    break;
                case "2":
                    $rootScope.userExists = true;
                    $rootScope.emailExists = false;
                    deferredObject.resolve({ success: false });
                    break;
                case "3":
                    $rootScope.emailExists = true;
                    $rootScope.userExists = true;
                    deferredObject.resolve({ success: false });
                    break;
                default:
                    $rootScope.emailExists = false;
                    $rootScope.userExists = false;
                    deferredObject.resolve({ success: true });
            }
            deferredObject.resolve({ success: true });
        }).error(function () {
            deferredObject.resolve({ success: false });
        });
        return deferredObject.promise;
    }
};
RegistrationFactory.$inject = ['$http', '$q', '$rootScope'];