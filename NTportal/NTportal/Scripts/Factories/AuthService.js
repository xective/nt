﻿'use strict';
var AuthService = function ($http, $q) {
    var serviceBase = 'http://localhost:2016/';
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        isAdmin: false,
        id:0,
        userName: ""
    };
    var _saveRegistration = function (registration) {
        //_logOut();
        //str = JSON.stringify(registration);
        
        console.log("hello");
        return $http.post(
             serviceBase+'/user/RegisterLogic', registration).then(function (response) {
                 var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
                 console.log("Response"+rsp);
                return response;
            });
    };
    var _getUserInfo = function () {
        $http.post(
             serviceBase + '/user/GetUserInfo').then(function (response) {
                 //var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
                 //console.log("Response" + rsp);
                 
                 return response;
             });
    }
    var _login = function (loginData) {
        //var rsp = JSON.stringify(loginData); // (Optional) beautiful indented output.
        //console.log("Login Data" + rsp);
        var deferred = $q.defer();
        $http.post(serviceBase + 'user/loginlogic', loginData).success(function (response) {
            var rsp = JSON.stringify(loginData, null, 4); // (Optional) beautiful indented output.
            console.log("loginnn" + rsp);
            var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
            console.log("login Response" + rsp);
            if (response.status == "success") {
                _authentication.isAuth = true;
                _authentication.userName = loginData.email;
                if (response.role == "admin") {
                    _authentication.isAdmin = true;
                }
                deferred.resolve(response);
            } else {
                deferred.resolve(response);
            }
            console.log("logged");
        }).error(function (err, status) {
            console.log("erroris");
            _logOut();

            deferred.reject(err);
        });
        return deferred.promise;
    };
    var _fillAuthData = function () {


        $http.post(
             serviceBase + '/user/AutoFill').then(function (response) {
                 var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
                 console.log("fill " + rsp);
                 if (response.data.status == "logOut") {
                     _authentication.isAuth = false;
                     _authentication.isAdmin = false;
                     _authentication.userName = "";

                    
                 } else {
                     _authentication.isAuth = true;
                     _authentication.userName = response.data.name;
                     if (response.data.role == "admin") {
                         _authentication.isAdmin = true;
                     }
                 }
                 return response;
             });
    }
    var _logOut = function () {

        //localStorageService.remove('authorizationData');
        console.log("logout");
        $http.post(
             serviceBase + '/user/LogOff').then(function (response) {
                 var rsp = JSON.stringify(response, null, 4); // (Optional) beautiful indented output.
                 console.log("Loggout" + rsp);
                 return response;
             });
        console.log("jau jau jau");
        _authentication.isAuth = false;
        _authentication.isAdmin = false;
        _authentication.userName = "";
    };
    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.getUserInfo = _getUserInfo;

    return authServiceFactory;
}
AuthService.$inject = ['$http', '$q'];