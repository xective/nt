﻿'use strict';
var SearchService = function ($http, $q) {
    var serviceBase = 'http://localhost:2016/';
    var searchServiceFactory = {};

    var _filter = {
        priceFrom: 0,
        priceTo: 0,
        areaFrom: 0,
        areaTo: 0,
        city: '',
        page: 1,
        roomFrom: '-',
        roomTo: '-',
        floorFrom: '-',
        floorTo:'-'
    }
    var _setFilterData = function (data) {
        /*var rsp = JSON.stringify(data, null, 4); // (Optional) beautiful indented output.
        console.log("Response" + rsp);*/
        _filter.priceFrom = data.priceFrom,
        _filter.priceTo = data.priceTo,
        _filter.areaFrom = data.areaFrom,
        _filter.areaTo = data.areaTo,
        _filter.city = data.city,
        _filter.roomFrom = data.roomFrom,
        _filter.roomTo = data.roomTo,
        _filter.floorFrom=data.floorFrom,
        _filter.floorTo=data.floorTo,
        _filter.type=data.type,
        _filter.goal=data.goal
    }
    var _getNotices = function (filterData) {
        var data = {
            page: filterData.page,
            city: filterData.city,
            priceFrom: filterData.priceFrom,
            priceTo: filterData.priceTo,
            areaFrom: filterData.areaFrom,
            areaTo: filterData.areaTo,
            roomFrom: filterData.roomFrom,
            roomTo: filterData.roomTo,
            floorFrom: filterData.floorFrom,
            floorTo: filterData.floorTo,
            type: filterData.type,
            goal: filterData.goal
        }
        var deferred = $q.defer();
        var config = {
            params: data,
            headers: { 'Accept': 'application/json' }
        };
        $http.get(serviceBase + 'notice/GetList', config).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get Notice Error")
            deferred.reject(err);
        });
        return deferred.promise;
    }

    var _getCount = function (data) {
        var deferred = $q.defer();
        $http.post(serviceBase + 'notice/GetListCount', data).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            console.log("Get Notice Error")
            deferred.reject(err);
        });
        return deferred.promise;
    };
    
    searchServiceFactory.setFilterData = _setFilterData;
    searchServiceFactory.filter = _filter;
    searchServiceFactory.getNotices = _getNotices;
    searchServiceFactory.getCount = _getCount;
    return searchServiceFactory;
}
SearchService.$inject = ['$http', '$q'];