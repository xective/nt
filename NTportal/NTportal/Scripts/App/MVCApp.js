﻿'use strict';
var MVCApp = angular.module('MVCApp', ['ngMaterial', 'ui.router', 'LocalStorageModule', 'ui.bootstrap']);

MVCApp.controller('LandingPageController', LandingPageController);
MVCApp.controller('LoginController', LoginController);
MVCApp.controller('RegistrationController', RegistrationController);
MVCApp.controller('HomePageController', HomePageController);
MVCApp.controller('UploadController', UploadController);
MVCApp.controller('SearchController', SearchController);
MVCApp.controller('NoticeController', NoticeController);
MVCApp.controller('UserController', UserController);
MVCApp.controller('MyNoticesController', MyNoticesController);
MVCApp.controller('UserListController', UserListController);
MVCApp.controller('AdminUserController', AdminUserController);
MVCApp.controller('UsersNoticesController', UsersNoticesController);

//MVCApp.factory('LoginFactory', LoginFactory);
//MVCApp.factory('RegistrationFactory', RegistrationFactory);
MVCApp.factory('AuthService', AuthService);
MVCApp.factory('SearchService', SearchService);
MVCApp.factory('UserService', UserService);
MVCApp.factory('NoticeService', NoticeService);
MVCApp.factory('AuthInterceptorService', AuthInterceptorService);



MVCApp.directive('stringToNumber', stringToNumber)

var configFunction = function ($stateProvider, $locationProvider, $urlRouterProvider, $mdThemingProvider, $httpProvider) {
    $mdThemingProvider.theme('default')
        .primaryPalette('grey')
        .accentPalette('indigo')
        .backgroundPalette('brown');
    $locationProvider.hashPrefix('!').html5Mode({
        enabled: true,
        requireBase: false
    });
    $urlRouterProvider.when('/home', '/newest').otherwise('/newest');
    $stateProvider.
        state('login', {
            url: "/login",
            templateUrl: '/user/login',
            controller: LoginController,
        })
        .state('profile', {
            url: "/profile",
            templateUrl: '/user/profilemain',
        })
        .state('profile.info', {
            url: "/info",
            templateUrl: '/user/profileinfo',
            controller: UserController
        })
        .state('profile.myNotices', {
            url: "/myNotices?page",
            templateUrl: '/home/Tab1',
            controller: MyNoticesController,
            params: {
                page: { value: '1', squash: true }
            }
        })
        .state('profile.info.edit', {
            url: "/edit",
            views: {
                '^.^.$default': {
                    templateUrl: '/user/profileedit',
                    controller: UserController
                }
            }
            //templateUrl: '/user/profileedit',
            //controller: UserController,
            
        })
        .state('upload', {
            url: "/upload",
            templateUrl: '/notice/upload',
            controller: UploadController
        })
        .state('register', {
            url: "/register",
            templateUrl: '/user/register',
            controller: RegistrationController
        }).state('noticeEdit', {
            url: "/notice/edit?{noticeId}",
            templateUrl: '/notice/edit',
            notice: function($stateParams) {
                return NoticeService.getNoticeData($stateParams.noticeId)
            },
            controller: NoticeController
        }).state('noticeShow', {
            url: "/notice/showinfo/{id:int}",
            views: {
                '^.$default': {
                    templateUrl: '/notice/show',
                    controller: NoticeController,
                    params: {
                        id: {}
                    }
                }
            }
            /*notice: function($stateParams) {
                return NoticeService.getNoticeData($stateParams.noticeId)
            },*/
            
        }).state('search', {
            url: '/search?page&city&priceFrom&priceTo'+
                '&areaFrom&areaTo&roomFrom&roomTo'+
                '&floorFrom&floorTo&type&goal',
            templateUrl: '/home/Tab1',
            controller: SearchController,
            params: {
                priceFrom: {value: '0', squash: true},
                priceTo: {value: '0', squash: true},
                areaFrom: {value: '0',squash: true},
                areaTo: {value: '0', squash: true},
                city: {value: '-',squash: true},
                type: { value: '-', squash: true },
                goal: { value: '-', squash: true },
                page: { value: '1', squash: true },
                roomFrom: { value: '-', squash: true },
                roomTo: { value: '-', squash: true },
                floorTo: { value: '-', squash: true },
                floorFrom: { value: '-', squash: true },

            }
        }).state('newest', {
            url: '/newest',
            onEnter: function () { console.log("enter tab1.html"); },
            controller: HomePageController,           
            templateUrl: '/home/Tab2'
        }).state('admin', {
            url: "/admin",
            templateUrl: '/user/adminmain'//,
            //controller: UploadController
        }).state('admin.userList', {
            url: "/users?page&name&role",

            templateUrl: '/user/UserList',
            controller: UserListController,
            params: {
                name: { value: '', squash: true },
                role: { value: '', squash: true },
                page: { value: '1', squash: true }
            }//,
            //controller: UploadController
        }).state('admin.userList.user', {
            url: "^/admin/userinfo/{id:int}",
            views: {
                '^.^.$default': {
                    templateUrl: '/user/AdminUserInfo',
                    controller: AdminUserController,
                    params: {
                        id: {}
                    }
                }
            }
        }).state('admin.userList.user.notices', {
            url: "^/admin/usernotices/{id:int}?page",
            params: {
                        page:'1'
                    },
            views: {
                '^.^.^.$default': {
                    templateUrl: '/user/UserNotices',
                    controller: UsersNoticesController,
                    params: {
                        id: {}
                    }
                }
            }
        }).state('admin.userList.user.edit', {
            url: "^/admin/useredit/{id:int}",
            views: {
                '^.^.^.$default': {
                    templateUrl: '/user/AdminUserEdit',
                    controller: AdminUserController,
                    params: {
                        id: {}
                    }
                }
            }
        });

    $httpProvider.interceptors.push('AuthInterceptorService');
}
configFunction.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', '$mdThemingProvider','$httpProvider'];
MVCApp.config(configFunction);

MVCApp.run(function ($http, $uiRouter,AuthService) {
    window['ui-router-visualizer'].visualizer($uiRouter);
    console.log("runing");
    AuthService.fillAuthData();
    //AuthService.getUserInfo();
});