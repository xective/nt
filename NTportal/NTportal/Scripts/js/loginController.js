﻿mvcApp.controller('loginController_main', function ($scope, $http, $location, LoginFactory) {
    var json = 'http://ipv4.myexternalip.com/json';
    $scope.submit = function () {
        if (!$scope.email.match('@')) {
            $scope.email_bad = true;
        } else {
            $scope.email_bad = false;
        }
        if ($scope.password.length < 8) {
            $scope.pass_short = true;
        } else {
            $scope.pass_short = false;
        }
        if (!$scope.password.match(/^(?=.*[0-9]).+$/)) {
            $scope.pass_not_number = true;
        } else {
            $scope.pass_not_number = false;
        }
        if ($scope.pass_short && $scope.pass_not_number) {
            $scope.pass_bad = true;
        } else {
            $scope.pass_bad = false;
        }

        if (!$scope.email_bad) {
            $scope.waiting = true;
            var result = LoginFactory($scope.email,  $scope.password);
            console.log(result)
            result.then(function (result) {
                if (result.success) {
                    window.location.pathname = '/Home/Index';
                    //$location.path('/routeOne');
                } else {
                    $scope.waiting = false;
                    //$scope.registerForm.registrationFailure = true;
                }
            });
        }
    };
})