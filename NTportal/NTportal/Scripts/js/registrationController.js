mvcApp.controller('registrationController_main', function ($scope, $http,$location, RegistrationFactory) {
    var json = 'http://ipv4.myexternalip.com/json';

    var ipAddress;
    (function () {
        //Bunch of code...
        $http.get(json)
            .success(function (data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
                // ipAddress = JSON.stringify(data.ip);
                ipAddress = data.ip;
            })
            .error(function (data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    })();
    $scope.submit = function () {
        if (!$scope.email.match('@')) {
            $scope.email_bad = true;
        } else {
            $scope.email_bad = false;
        }
        if ($scope.password.length < 8) {
            $scope.pass_short = true;
        } else {
            $scope.pass_short = false;
        }
        if (!$scope.password.match(/^(?=.*[0-9]).+$/)) {
            $scope.pass_not_number = true;
        } else {
            $scope.pass_not_number = false;
        }
        if ($scope.pass_short && $scope.pass_not_number) {
            $scope.pass_bad = true;
        } else {
            $scope.pass_bad = false;
        }
        if ($scope.password != $scope.reenter_password) {
            $scope.pass_not_match = true;
        } else {
            $scope.pass_not_match = false;
        }
        if (!$scope.email_bad && !$scope.pass_bad && !$scope.pass_not_match) {
            $scope.waiting = true;
            var result = RegistrationFactory($scope.email, $scope.username, $scope.password, ipAddress, $scope.userExists, $scope.emailExists);
            console.log(result)
            result.then(function (result) {
                if (result.success) {
                    window.location.pathname = '/Home/Index';
                    //$location.path('/routeOne');
                } else {
                    $scope.waiting = false;
                    //$scope.registerForm.registrationFailure = true;
                }
            });
        }
    };
})