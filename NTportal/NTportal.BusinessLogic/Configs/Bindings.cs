﻿using Ninject.Modules;
using Ninject;
using NTportal.BusinessLogic;

public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IRepository>().To<Repository>();
        }
    }

