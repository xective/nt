﻿using NTportal.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.BusinessLogic.Interfaces
{
    public interface INoticeService
    {
        string Create(NoticeModel noticeModel);
        string Delete(int id);
        string Edit(NoticeModel noticeModel);
    }
}
