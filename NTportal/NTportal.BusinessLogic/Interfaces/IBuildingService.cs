﻿using NTportal.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.BusinessLogic.Interfaces
{
    public interface IBuildingService
    {
        string Create(BuildingModel buildingModel);
        string Delete(int id);
        string Edit(BuildingModel buildingModel);
    }
}
