﻿using NTportal.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.BusinessLogic.Interfaces
{
    public interface IAddressService
    {
        string Create(AddressModel addressModel);
        string Delete(int id);
        string Edit(AddressModel addressModel);
    }
}
