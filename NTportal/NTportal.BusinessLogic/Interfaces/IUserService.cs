﻿using NTportal.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.BusinessLogic.Interfaces
{
    public interface IUserService
    {
        ClaimsIdentity Authorise(LoginModel user);

        string Register(UserModel user);

        //void AddRolesToClaimList(List<Claim> claims, IEnumerable<string> roles);
    }
}
