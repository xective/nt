﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.BusinessLogic.Interfaces
{
    public interface ILoggerService
    {
        string LogDisplays(List<int> noticeIds, bool isKnownUser, int guestOrUserID);
    }
}
