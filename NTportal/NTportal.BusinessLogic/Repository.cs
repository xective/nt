﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTportal.Data.Models;
using NTportal.Data.DatabaseContext;
using NTportal.Data.Entities;
using System.Web.Mvc;
using System.Data.Entity.SqlServer;

namespace NTportal.BusinessLogic
{
    public class Repository : IRepository
    {
        public MySqlDbContext db { get; private set; }
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0,
                                                      DateTimeKind.Utc);
        public Repository(MySqlDbContext db)
        {
            this.db = db;
        }
        public List<User> GetUsers()
        {
            List<User> users = db.Users.ToList();
            return users;
        }

        public JsonResult GetNewest()
        {
            var results = db.Notices.Select(x => new
            {
                Name = x.name,
                Price = x.price,
                City = x.city,
                Area = x.area,
                Id = x.id,
                CreateDate = x.createdOn,
                Description = x.description,
                Floor = (int?)x.buildings.FirstOrDefault().floors ?? 0,
                RoomCount = x.buildings.ToList().Count == 0 ? 0 : x.buildings.Sum(y => y.rooms)
            }).OrderByDescending(y => y.CreateDate).Take(6).ToList();
            return new JsonResult() { Data = results, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public int GetNewestNoticeId()
        {
            var results = db.Notices.Select(x => new
            {
                Id = x.id,
                CreateDate = x.createdOn
            }).OrderByDescending(y => y.CreateDate).Take(1).ToList();
            return results[0].Id;
        }

        public int GetNewestAddressId()
        {
            var results = db.Addresses.Select(x => new
            {
                Id = x.id
            }).OrderByDescending(y => y.Id).Take(1).ToList();
            return results[0].Id;
        }

        public JsonResult GetUserInfo(int id)
        {
            //var unixEpoch = DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var check = db.Users.Where(x => x.id == id).ToList();
            var results = db.Users.Where(x => x.id == id).Select(x => new
            {
                UserName=x.Username,
                Email=x.Email,
                Role=x.Role,
                CreationDate=x.createdOn,
                Id=x.id,
                //DateCreation = SqlFunctions.DateDiff("ss", Epoch, x.createdOn),
                /*AditionalInfo = x.Address == null ? 0 : new
                {
                    City = x.Address.City,
                    Country = x.Address.Country,
                    Street = x.Address.Street,
                    StreetNo = x.Address.BuildingNumber,
                    AddressId = x.Address.id
                }*/
                 AditionalInfo = new { 
                     City =x.Address.City ??"",
                     Country=x.Address.Country??"",
                     Street=x.Address.Street??"",
                     StreetNo=x.Address.BuildingNumber??"",
                     AddressId=(int?)x.Address.id??0
                 }
            }).First();
            //System.DateTime dateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            //dateTime = dateTime.AddSeconds(results.);
            return new JsonResult() { Data = results, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public JsonResult GetNotice(int id)
        {
            var results = db.Notices.Where(x => x.id == id).Select(x => new
            {
                Id = x.id,
                NoticeType = x.noticeType,
                Goal = x.goal,
                Price = x.price,
                Name = x.name,
                Purpose = x.purpose,
                CreateDate = x.createdOn,
                Description=x.description,
                Number=x.number,
                Email=x.email,
                Area=x.area,
                //LandPurpose=x.landPurpose,
                City=x.city,
                Buildings = x.buildings.Select(y =>new
                {
                    FloorCount=y.floors,
                    Area=y.totalArea,
                    BuildingDate=y.constructionFrom,
                    Purpose=y.purpose,
                    RoomCount=y.rooms,
                    Heating=y.heating,
                    Conveniences=y.conveniences,
                    Condition=y.condition,
                    HasWater=y.hasWater,
                    HasPlumbing=y.hasPlumbing,
                    Country=y.Address.Country,
                    City=y.Address.City,
                    Street=y.Address.Street,
                    StreetNo=y.Address.BuildingNumber
                })
            }).ToList();
            return new JsonResult() { Data = results, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public JsonResult GetUsers(string name,string role ,int offset)
        {
            IQueryable<User> DBset = db.Users;
            if (name != null)
            {
                DBset = DBset.Where(x => x.Username.Contains(name) || x.Email.Contains(name));
            }
            if(role != null)
            {
                DBset = DBset.Where(x => x.Role.Contains(role));
            }
            DBset = DBset.OrderByDescending(z => z.createdOn).Skip(offset * 9).Take(9);
            var results = DBset.Select(x => new
            {
                UserName = x.Username,
                Email = x.Email,
                NoticeCount = (int?)x.Notices.Count ?? 0,
                CreationDate = x.createdOn,
                Id=x.id,
                Role=x.Role
            }).ToList();
            return new JsonResult() { Data = results, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public JsonResult GetNoticesFiltered(FilterModel filter, int offset)
        {
            var Dbset = db.Notices;
            IQueryable<Notice> DBset = db.Notices;
            if (filter.City != null && filter.City != "-")
            {
                DBset = DBset.Where(x => x.city.Contains(filter.City));
                //results = results.Where(x => x.Price < filter.PriceTo).ToList();
            }
            if (filter.Type != null && filter.Type != "-")
            {
                DBset = DBset.Where(x => x.noticeType.Contains(filter.Type));
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.Goal > 0)
            {
                int goal = filter.Goal - 1;
                DBset = DBset.Where(x => (int)x.goal == goal);
            }
            if (filter.PriceFrom > 0)
            {
                DBset = DBset.Where(x => x.price >= filter.PriceFrom);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.PriceTo > 0)
            {
                DBset = DBset.Where(x => x.price <= filter.PriceTo);
                //results = results.Where(x => x.Price < filter.PriceTo).ToList();
            }
            
            if (filter.RoomCountFrom > 0)
            {
                DBset = DBset.Where(x => x.buildings.Sum(y => y.rooms) >= filter.RoomCountFrom);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.RoomCountTo > 0)
            {
                DBset = DBset.Where(x => x.buildings.Sum(y => y.rooms) <= filter.RoomCountTo);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.FlatSizeFrom > 0)
            {
                DBset = DBset.Where(x => x.buildings.Sum(y => y.totalArea) >= filter.FlatSizeFrom);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.FlatSizeTo > 0)
            {
                DBset = DBset.Where(x => x.buildings.Sum(y => y.totalArea) <= filter.FlatSizeTo);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.SizeTo > 0)
            {
                DBset = DBset.Where(x => x.area <= filter.SizeTo);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.SizeFrom > 0)
            {
                DBset = DBset.Where(x => x.area >= filter.SizeFrom);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.FloorFrom > 0)
            {
                DBset = DBset.Where(x => x.buildings.FirstOrDefault().floors >= filter.FloorFrom);
            }
            if (filter.FloorTo > 0)
            {
                DBset = DBset.Where(x => x.buildings.FirstOrDefault().floors <= filter.FloorTo);
            }
            DBset = DBset.OrderBy(z => z.createdOn).Skip(offset * 9).Take(9);
            var results = DBset.Select(x => new
            {
                Name = x.name,
                Price = x.price,
                City = x.city,
                Area = x.area,
                Id = x.id,
                NoticeType = x.noticeType,
                Goal = x.goal,
                CreateDate = x.createdOn,
                Description = x.description,
                Floor = (int?)x.buildings.FirstOrDefault().floors ?? 0,
                RoomCount = x.buildings.ToList().Count==0 ? 0 : x.buildings.Sum(y => y.rooms) 
            }).ToList();
            return new JsonResult() { Data = results, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public User GetUserByEmail(string email)
        {
            return db.Users.FirstOrDefault(x => x.Email == email);
        }

        public Notice GetNoticeById(int id)
        {
            return db.Notices.FirstOrDefault(x => x.id == id);
        }

        public Address GetAddressById(int id)
        {
            return db.Addresses.FirstOrDefault(x => x.id == id);
        }

        public User GetUserByUsername(string username)
        {
            return db.Users.FirstOrDefault(x => x.Username == username);
        }

        public string InsertUser(User user)
        {
            string response = "";
            try
            {
                db.Users.Add(user);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                response = e.Message;
            }

            return response;
        }
        public string AditionalUser(AddressChangeModel address)
        {
            User original = GetUserByEmail(address.Email);
            if (address.Role != null)
            {
                original.Role = address.Role.ToUpperInvariant();
            }
            if ( address.AddressId != 0)
            {
                Address originAddress = db.Addresses.First(x => x.id == address.AddressId);
                originAddress.City = address.City;
                originAddress.Country = address.Country;
                originAddress.Street = address.Street;
                originAddress.BuildingNumber = address.StreetNo;
                db.SaveChanges();
            }else
            {
                
                int userId = original.id;
                //new NoticeViewModel { Area = 1.9, Buildings = new List<Building>(), CenterLatitude = 18.8, CenterLongitude = 18.8, City = "Kaunas", CreatedOn = DateTime.Now, Description = "desc", Email = "audfra@ktu.lt", Goal = Data.Entities.goals.sale, LandMountainousness = "yes", LandPurpose = "none", Name = "insertTest", NoticeType = "type", Number = "123456789", Price = 99.99, Purpose = Data.Entities.purposes.offer, User = user };
                
                Address myAddress = new Address { Country = address.Country, Region = "regionas", City = address.City, Street = address.Street,
                    BuildingNumber = address.StreetNo, createdOn = DateTime.Now};
                db.Addresses.Add(myAddress);
                original.Address = myAddress;
                db.SaveChanges();
                //var results = db.Users.Where(x => x.id == original.id).First();
            }
            /*User original = db.Users.First(u => u.id == user.id);
            if (original == null) return "Not found";
            original.Address = user.Address;
            original.createdOn = user.createdOn;
            original.Email = user.Email;
            original.Info = user.Info;
            original.IpAddress = user.IpAddress;
            original.IsDeleted = user.IsDeleted;
            original.Notices = user.Notices;
            original.Password = user.Password;
            original.Role = user.Role;
            original.Username = user.Username;
            db.SaveChanges();*/
            return "";
        }
        public string EditUser(User user)
        {
            User original = db.Users.First(u => u.id == user.id);
            if (original == null) return "Not found";
            original.Address = user.Address;
            original.createdOn = user.createdOn;
            original.Email = user.Email;
            original.Info = user.Info;
            original.IpAddress = user.IpAddress;
            original.IsDeleted = user.IsDeleted;
            original.Notices = user.Notices;
            original.Password = user.Password;
            original.Role = user.Role;
            original.Username = user.Username;
            db.SaveChanges();
            return "";
        }

        public string InsertNotice(Notice notice)
        {
            string response = "";
            try
            {
                db.Notices.Add(notice);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                response = e.Message;
            }

            return response;
        }

        public string InsertAddress(Address address)
        {
            string response = "";
            try
            {
                db.Addresses.Add(address);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                response = e.Message;
            }

            return response;
        }

        public string EditAddress(Address address)
        {
            Address original = db.Addresses.First(n => n.id == address.id);
            if (original == null) return "Not found";
            original.Country = address.Country;
            original.Region = address.Region;
            original.City = address.City;
            original.createdOn = address.createdOn;
            original.Street = address.Street;
            original.BuildingNumber = address.BuildingNumber;
            db.SaveChanges();
            return "";
        }

        public string DeleteAddress(int id)
        {
            try
            {
                db.Addresses.Remove(db.Addresses.First(n => n.id == id));
                db.SaveChanges();
                return "";
            }
            catch (Exception e)
            {
                return "Remove failed";
            }
        }

        public string EditNotice(Notice notice)
        {
            Notice original = db.Notices.First(n => n.id == notice.id);
            if (original == null) return "Not found";
            original.area = notice.area;
            original.buildings = notice.buildings;
            original.city = notice.city;
            original.centerLatitude = notice.centerLatitude;
            original.centerLongitude = notice.centerLongitude;
            original.createdOn = notice.createdOn;
            original.description = notice.description;
            original.goal = notice.goal;
            original.email = notice.email;
            original.landMountainousness = notice.landMountainousness;
            original.landPurpose = notice.landPurpose;
            original.name = notice.name;
            original.noticeType = notice.noticeType;
            original.number = notice.number;
            original.price = notice.price;
            original.purpose = notice.purpose;
            db.SaveChanges();
            return "";
        }

        public string DeleteNotice(int id)
        {
            try
            {
                db.Notices.Remove(db.Notices.First(n => n.id == id));
                db.SaveChanges();
                return "";
            }
            catch (Exception e)
            {
                return "Remove failed";
            }
        }

        public List<Notice> GetNotices(bool isASC, int count)
        {
            if (isASC)
                return db.Notices.OrderBy(x => x.name).Take(count).ToList();

            return db.Notices.OrderByDescending(x => x.name).Take(count).ToList();
        }

        public List<Notice> GetUserNotices(int userId)
        {
            return db.Notices.Where(n => n.userID == userId).ToList();
        }

        public Notice Notice(int noticeId)
        {
            return db.Notices.FirstOrDefault(n => n.id == noticeId);
        }

        public Task<List<Notice>> GetNoticesAsync(bool isASC, int count)
        {
            throw new NotImplementedException();
        }

        public string InsertView(View view)
        {
            string response = "";
            try
            {
                db.Views.Add(view);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                response = e.Message;
            }

            return response;
        }

        public string InsertDisplay(Display display)
        {
            string response = "";
            try
            {
                db.Displays.Add(display);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                response = e.Message;
            }

            return response;
        }

        public string InsertDisplays(List<Display> displays)
        {
            string response = "";
            try
            {
                db.Displays.AddRange(displays);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                response = e.Message;
            }

            return response;
        }

        public Building GetBuilding(int id)
        {
            return db.Buildings.FirstOrDefault(b => b.BuildingID == id);
        }

        public string InsertBuilding(Building building) {
            string response = "";
            try
            {
                db.Buildings.Add(building);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                response = e.Message;
            }

            return response;
        }

        public string EditBuilding(Building building)
        {
            Building original = db.Buildings.First(n => n.BuildingID == building.BuildingID);
            if (original == null) return "Not found";
            original.Address = building.Address;
            original.addressID = building.addressID;
            original.BuildingID = building.BuildingID;
            original.category = building.category;
            original.condition = building.condition;
            original.constructionFrom = building.constructionFrom;
            original.constructionTo = building.constructionTo;
            original.conveniences = building.conveniences;
            original.floors = building.floors;
            original.hasPlumbing = building.hasPlumbing;
            original.hasWater = building.hasWater;
            original.heating = building.heating;
            original.layout = building.layout;
            original.notice = building.notice;
            original.noticeID = building.noticeID;
            original.purpose = building.purpose;
            original.rooms = building.rooms;
            original.totalArea = building.totalArea;
            original.type = building.type;
            db.SaveChanges();
            return "";
        }

        public string DeleteBuilding(int id) {
            try
            {
                db.Buildings.Remove(db.Buildings.First(n => n.BuildingID == id));
                db.SaveChanges();
                return "";
            }
            catch (Exception e)
            {
                return "Remove failed";
            }
        }


        public List<Notice> GetFilteredNotices(bool isASC, int count, string filterValue)
        {
            var entities = db.Notices.Where(x => x.name.Contains(filterValue) || x.description.Contains(filterValue) || x.email.Contains(filterValue) ||
            (x.price != null ? x.price.ToString().Contains(filterValue) : false) || x.area.ToString().Contains(filterValue) || x.landMountainousness.Contains(filterValue) ||
            x.landPurpose.Contains(filterValue) || x.centerLatitude.ToString().Contains(filterValue) || x.centerLongitude.ToString().Contains(filterValue) || x.number.Contains(filterValue) ||
            x.noticeType.Contains(filterValue)).ToList();

            if (entities.Count > 0)
            {
                if (isASC)
                    return entities.OrderBy(x => x.name).Take(count).ToList();

                return entities.OrderByDescending(x => x.name).Take(count).ToList();
            }

            return null;
        }
        public int GetMyListCount(string email)
        {
            User user = GetUserByEmail(email);
            if (user != null)
            {
                var Dbset = db.Notices.Where(x => x.userID == user.id).Select(y => new
                {
                    Name = y.name
                }
            ).ToList();
                return Dbset.Count;
            }
            return 0;
        }
        public int GetUsersListCount(int id)
        {
            IQueryable<Notice> DBset = db.Notices;
            if (id > 0)
            {
                DBset=DBset.Where(x => x.userID == id);
            }
            var results = DBset.Select(y => new
            {
                Name = y.name
            }).ToList();
            return results.Count;
        }
        public JsonResult GetUsersList(int id,int offset)
        {
            IQueryable<Notice> DBset = db.Notices;
            if (id > 0)
            {
                DBset=DBset.Where(x => x.userID == id);
            }
            DBset = DBset.OrderBy(z => z.createdOn).Skip(offset * 9).Take(9);
            var results = DBset.Select(x => new
            {
                Name = x.name,
                Price = x.price,
                City = x.city,
                Area = x.area,
                Id = x.id,
                NoticeType = x.noticeType,
                Goal = x.goal,
                CreateDate = x.createdOn,
                Description = x.description,
                Floor = (int?)x.buildings.FirstOrDefault().floors ?? 0,
                RoomCount = x.buildings.ToList().Count == 0 ? 0 : x.buildings.Sum(y => y.rooms)
            }).ToList();
            return new JsonResult() { Data = results, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public int GetUserCount(string name)
        {
            IQueryable<User> DBset = db.Users;
            if (name != null)
            {
                DBset = DBset.Where(x => x.Username.Contains(name) || x.Email.Contains(name));
            }
            var results = DBset.Select(y => new
            {
                name = y.Email
            }).ToList().Count;
            return results;
        }
        public JsonResult GetMyList(string email,int offset)
        {
            User user = GetUserByEmail(email);
            IQueryable<Notice> DBset = db.Notices;
            DBset = DBset.Where(x => x.userID == user.id);
            var results = DBset.Select(y => new
            {
                Name = y.name,
                Price = y.price,
                City = y.city,
                Area = y.area,
                Id = y.id,
                NoticeType=y.noticeType,
                Goal=y.goal,
                Date = y.createdOn,
                Floor = (int?)y.buildings.FirstOrDefault().floors??0,
                RoomCount = y.buildings.ToList().Count == 0 ? 0 : y.buildings.Sum(z => z.rooms)
            }
            ).OrderBy(z => z.Date).Skip(offset * 9).Take(9).ToList();
            return new JsonResult() { Data = results, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public int GetNoticesFilteredCount(FilterModel filter)
        {
            var Dbset = db.Notices;
            IQueryable<Notice> DBset = db.Notices;
            if (filter.Type != null && filter.Type!="-")
            {
                DBset = DBset.Where(x => x.noticeType.Contains(filter.Type));
            }
            if (filter.City != null && filter.City!="-")
            {
                DBset = DBset.Where(x => x.city.Contains(filter.City));
            }
            if (filter.Goal > 0)
            {
                int goal = filter.Goal - 1;
                DBset = DBset.Where(x => (int)x.goal==goal);
            }
            if (filter.PriceFrom > 0)
            {
                DBset = DBset.Where(x => x.price >= filter.PriceFrom);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.PriceTo > 0)
            {
                DBset = DBset.Where(x => x.price <= filter.PriceTo);
                //results = results.Where(x => x.Price < filter.PriceTo).ToList();
            }
            
            if (filter.RoomCountFrom > 0)
            {
                DBset = DBset.Where(x => x.buildings.Sum(y => y.rooms) >= filter.RoomCountFrom);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.RoomCountTo > 0)
            {
                DBset = DBset.Where(x => x.buildings.Sum(y => y.rooms) <= filter.RoomCountTo);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.FlatSizeFrom > 0)
            {
                DBset = DBset.Where(x => x.buildings.Sum(y => y.totalArea) >= filter.FlatSizeFrom);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.FlatSizeTo > 0)
            {
                DBset = DBset.Where(x => x.buildings.Sum(y => y.totalArea) <= filter.FlatSizeTo);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.SizeTo > 0)
            {
                DBset = DBset.Where(x => x.area <= filter.SizeTo);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.SizeFrom > 0)
            {
                DBset = DBset.Where(x => x.area >= filter.SizeFrom);
                //Dbset = Dbset.Where(x => x.price >= filter.PriceFrom);
                //results=results.Where(x => x.Price > filter.PriceFrom).ToList();
            }
            if (filter.FloorFrom > 0)
            {
                DBset = DBset.Where(x => x.buildings.FirstOrDefault().floors>=filter.FloorFrom);
            }
            if (filter.FloorTo > 0)
            {
                DBset = DBset.Where(x => x.buildings.FirstOrDefault().floors <= filter.FloorTo);
            }
            var results = DBset.Select(x => new
            {
                Name = x.name
            }).ToList();
            return results.Count;
        }
    }
}
