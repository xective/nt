﻿using NTportal.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTportal.Data.Models;
using System.Security.Claims;
using NTportal.Data.DatabaseContext;
using Microsoft.AspNet.Identity;
using NTportal.Data.Entities;

namespace NTportal.BusinessLogic.Implementation
{
    public class UserService : IUserService
    {


        public IRepository Repository { get; private set; }
        

        public UserService(IRepository repo)
        {
            this.Repository = repo;
        }



        public ClaimsIdentity Authorise(LoginModel user)
        {


            var dbUser = Repository.GetUserByEmail(user.Email);

            if (dbUser == null)
                return null;
            if (dbUser.Password != user.Password)
                return null;

            var list = new List<Claim>{
                // adding following 2 claim just for supporting default antiforgery provider
                new Claim(ClaimTypes.NameIdentifier, user.Email),
                new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "ASP.NET Identity", "http://www.w3.org/2001/XMLSchema#string"),
                new Claim(ClaimTypes.Name, user.Email),
                new Claim(ClaimTypes.Role, dbUser.Role)
            };
            
            //AddRolesToClaimList(list, dbUser.role);
            return new ClaimsIdentity(
                  //list.ToArray(),
                  list,
                  DefaultAuthenticationTypes.ApplicationCookie);
            }

        public string Register(UserModel user)
        {
            var dbUser = Repository.GetUserByEmail(user.Email);
            var dbUser1 = Repository.GetUserByUsername(user.UserName);
            if (dbUser == null && dbUser1 == null)
            {
                var dbRsp = Repository.InsertUser((User)user);
                return (String.IsNullOrWhiteSpace(dbRsp) ? "" : ("Problems with database insert action: " + dbRsp));
            }
            else if (dbUser != null && dbUser1 != null)
            {
                return "3";
            } else if (dbUser1 != null) {
                return "2";
             } 

            return "1";
        }

        //public void AddRolesToClaimList(List<Claim> claims, IEnumerable<string> roles)
        //{
        //    foreach (var role in roles)
        //        claims.Add(new Claim(ClaimTypes.Role, role));
        //}
    }
}
