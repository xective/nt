﻿using NTportal.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTportal.Data.Models;
using NTportal.Data.Entities;

namespace NTportal.BusinessLogic.Implementation
{
    public class AddressService : IAddressService
    {
        public IRepository Repository { get; private set; }


        public AddressService(IRepository repo)
        {
            this.Repository = repo;
        }
        public string Create(AddressModel address)
        {
            var dbRsp = Repository.InsertAddress((Address)address);
            return (String.IsNullOrWhiteSpace(dbRsp) ? "" : ("Problems with database insert action: " + dbRsp));
        }

        public string Delete(int id)
        {
            var dbRsp = Repository.DeleteAddress(id);
            return (String.IsNullOrWhiteSpace(dbRsp) ? "" : ("Problems with database insert action: " + dbRsp));
        }

        public string Edit(AddressModel address)
        {
            var dbRsp = Repository.EditAddress((Address)address);
            return (String.IsNullOrWhiteSpace(dbRsp) ? "" : ("Problems with database insert action: " + dbRsp));
        }
    }
}
