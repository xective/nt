﻿using NTportal.BusinessLogic.Interfaces;
using NTportal.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.BusinessLogic.Implementation
{
    public class LoggerService : ILoggerService
    {
        public IRepository Repository { get; private set; }


        public LoggerService(IRepository repo)
        {
            this.Repository = repo;
        }

        public string LogDisplays(List<int> noticeIds, bool isKnownUser, int guestOrUserID)
        {
            List<Display> displays = new List<Display>();

            foreach (var id in noticeIds)
                if (isKnownUser)
                    displays.Add(new Display {
                        PublishmentID = id,
                        IsKnownUser = isKnownUser,
                        UserID = guestOrUserID
                    });
               else
                    displays.Add(new Display
                    {
                        PublishmentID = id,
                        IsKnownUser = isKnownUser,
                        GuestID = guestOrUserID
                    });

            var dbRsp = Repository.InsertDisplays(displays);
            return (String.IsNullOrWhiteSpace(dbRsp) ? "" : ("Problems with database insert action: " + dbRsp));
        }




    }
}
