﻿using NTportal.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTportal.Data.Models;
using NTportal.Data.Entities;

namespace NTportal.BusinessLogic.Implementation
{
    public class BuildingService : IBuildingService
    {
        public IRepository Repository { get; private set; }


        public BuildingService(IRepository repo)
        {
            this.Repository = repo;
        }
        public string Create(BuildingModel building)
        {
            var dbRsp = Repository.InsertBuilding((Building)building);
            return (String.IsNullOrWhiteSpace(dbRsp) ? "" : ("Problems with database insert action: " + dbRsp));
        }

        public string Delete(int id)
        {
            var dbRsp = Repository.DeleteBuilding(id);
            return (String.IsNullOrWhiteSpace(dbRsp) ? "" : ("Problems with database insert action: " + dbRsp));
        }

        public string Edit(BuildingModel building)
        {
            var dbRsp = Repository.EditBuilding((Building)building);
            return (String.IsNullOrWhiteSpace(dbRsp) ? "" : ("Problems with database insert action: " + dbRsp));
        }
    }
}
