﻿using NTportal.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTportal.Data.Models;
using NTportal.Data.Entities;

namespace NTportal.BusinessLogic.Implementation
{
    public class NoticeService : INoticeService
    {
        public IRepository Repository { get; private set; }


        public NoticeService(IRepository repo)
        {
            this.Repository = repo;
        }
        public string Create(NoticeModel notice)
        {
            var dbRsp = Repository.InsertNotice((Notice)notice);
            return (String.IsNullOrWhiteSpace(dbRsp) ? "" : ("Problems with database insert action: " + dbRsp));
        }

        public string Delete(int id)
        {
            var dbRsp = Repository.DeleteNotice(id);
            return (String.IsNullOrWhiteSpace(dbRsp) ? "" : ("Problems with database insert action: " + dbRsp));
        }

        public string Edit(NoticeModel notice)
        {
            var dbRsp = Repository.EditNotice((Notice)notice);
            return (String.IsNullOrWhiteSpace(dbRsp) ? "" : ("Problems with database insert action: " + dbRsp));
        }
    }
}
