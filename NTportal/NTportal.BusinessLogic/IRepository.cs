﻿using NTportal.Data.Entities;
using NTportal.Data.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace NTportal.BusinessLogic
{
    public interface IRepository
    {
        //User methods
        User GetUserByUsername(string username);
        User GetUserByEmail(string email);

        string InsertUser(User user);

        string EditUser(User user);
        string AditionalUser(AddressChangeModel address);

        List<User> GetUsers();







        //Notices methods
        string InsertNotice(Notice notice);

        string EditNotice(Notice notice);

        string DeleteNotice(int id);
        int GetMyListCount(string email);
        int GetUserCount(string name);
        int GetUsersListCount(int id);
        JsonResult GetUsersList(int id, int offset);

        JsonResult GetNoticesFiltered(FilterModel filter,int offset);
        int GetNoticesFilteredCount(FilterModel filter);

        JsonResult GetNewest();
        JsonResult GetMyList(string email,int offset);

        int GetNewestNoticeId();

        int GetNewestAddressId();

        Notice GetNoticeById(int id);

        Address GetAddressById(int id);

        JsonResult GetUsers(string name,string role, int page);

        JsonResult GetNotice(int id);

        JsonResult GetUserInfo(int id);

        List<Notice> GetNotices(bool isASC, int count);

        List<Notice> GetFilteredNotices(bool isASC, int count, string filterValue);

        List<Notice> GetUserNotices(int userId);

        Notice Notice(int noticeId);

        Task<List<Notice>> GetNoticesAsync(bool isASC, int count);





        //View methods
        string InsertView(View view);




        //Display methods
        string InsertDisplay(Display display);
        string InsertDisplays(List<Display> display);

        //Building methods
        Building GetBuilding(int id);

        string InsertBuilding(Building building);
        string EditBuilding(Building building);
        string DeleteBuilding(int id);

        //Address methods
        string InsertAddress(Address address);

        string EditAddress(Address address);

        string DeleteAddress(int id);
    }
}
