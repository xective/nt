﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NTportal.Data.Entities
{
    [Table("PASTATAS")]
    public class Building
    {
        [Key]
        public int BuildingID { get; set; }
        [Required]
        [Column("fk_ADRESAS")]        
        public int addressID { get; set; }
        public virtual Address Address { get; set; }
        [Required]
        [Column("fk_SKELBIMAS")]
        public int noticeID { get; set; }
        public virtual Notice notice { get; set; }
        [Required]
        [Column("statymo_data")]
        public DateTime constructionFrom { get; set; }
        [Required]
        [Column("statymo_iki")]
        public DateTime constructionTo { get; set; }
        [Required]
        [Column("bendras_plotas", TypeName = "double")]
        public double totalArea { get; set; }
        [Required]
        [Column("aukstu_skaicius")]
        public int floors { get; set; }
        [Required]
        [Column("kambariai", TypeName = "int")]
        public int rooms { get; set; }
        [Column("patogumai", TypeName = "varchar")]
        [MaxLength(255)]
        public string conveniences { get; set; }
        [Column("isplanavimas", TypeName = "varchar")]
        [MaxLength(100)]
        public string layout { get; set; }
        [Required]
        [Column("paskirtis", TypeName = "varchar")]
        [MaxLength(30)]
        public string purpose { get; set; }
        [Required]
        [Column("ar_yra_vadentekis")]
        public bool hasWater { get; set; }
        [Required]
        [Column("ar_yra_kanalizacija")]
        public bool hasPlumbing { get; set; }
        [Required]
        [Column("bukle")]
        public conditions condition { get; set; }
        [Required]
        [Column("kategorija")]
        public categories category { get; set; }
        [Required]
        [Column("tipas")]
        public types type { get; set; }
        [Required]
        [Column("sildymas")]
        public heatingType heating { get; set; }
    }

    public enum conditions {furnished, partial_furnishings, under_construction, needs_renovation }
    public enum categories {office, house, flat, outbuilding, commercial_building, other }
    public enum types {log, brick, carcass, monolith, block, wood, other }
    public enum heatingType {automatic, gas, central_heating, central_collector, electric, combined, geothermal, local_boiler, furnace, unheated, other }
}
