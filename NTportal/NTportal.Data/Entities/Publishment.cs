﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NTportal.Data.Entities
{
    [Table("PASKELBIMAS")]
    public class Publishment1 : EntityBase
    {
        [Required]
        [Column("fk_SKELBIMASid")]
        public int noticeID { get; set; }
        public virtual Notice notice { get; set; }
        [Required]
        [Column("data_nuo")]
        public DateTime dateFrom { get; set; }
        [Required]
        [Column("data_iki")]
        public DateTime dateTo { get; set; }
        /*[Column("fk_SKELBIMASid")]
        public virtual ICollection<Display> displays { get; set; }
        [Column("fk_PIRMIENYBEid")]
        public virtual ICollection<Priority> priorities { get; set; }*/
    }
}
