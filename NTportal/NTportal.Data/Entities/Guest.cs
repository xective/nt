﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NTportal.Data.Entities
{
    [Table("ANONIMAS")]
    public class Guest : EntityBase
    {
        [Required]
        [Column("ip_adresas", TypeName = "varchar")]
        [MaxLength(30)]
        public string ip { get; set; }
        [Required]
        [Column("apsilankymo_data")]
        public DateTime visitDate { get; set; }
        [Required]
        [Column("apsilankymo_trukme")]
        public int visitDuration { get; set; }


        //[Column("fk_PARODYMAI")]
        //public ICollection<Display> displays { get; set; }
    }
}
