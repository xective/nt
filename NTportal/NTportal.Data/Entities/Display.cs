﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NTportal.Data.Entities
{
    [Table("PARODYMAS")]
    public class Display : EntityBase
    {
        [Column("fk_PASKELBIMASid")]
        public int PublishmentID { get; set; }
        [Required]
        [Column("ar_yra_vartotojas")]
        public bool IsKnownUser { get; set; }
        [Column("fk_guest_ID")]
        public int? GuestID { get; set; }
        [Column("fk_user_ID")]
        public int? UserID { get; set; }


        /*public virtual Guest Guest { get; set; }
        public virtual View View { get; set; }
        public virtual Publishment Publishment { get; set; }*/
    }
}
