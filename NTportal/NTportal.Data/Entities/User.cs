﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NTportal.Data.Entities
{
    [Table("PRISIJUNGIMAS")]
    public class User : EntityBase
    {
        [Column("slapyvardis")]
        [MaxLength(20)]
        public string Username { get; set; }

        [Required]
        [Column("slaptazodis")]
        [MaxLength(20)]
        public string Password { get; set; }

        [Required]
        [Column("prisijungimo_el_pastas")]
        [MaxLength(30)]
        public string Email { get; set; }
        
        [Required]
        [Column("role")]
        [MaxLength(20)]
        public virtual string Role { get; set; }

        [Column("fk_ADRESAS")]
        public Address Address { get; set; } //= new Address();

        [Column("fk_VARTOTOJO_INFORMACIJAid")]
        public virtual AdditionalInfo Info { get; set; } //= new AdditionalInfo();

        [Required]
        [Column("ip_adresas")]
        [MaxLength(30)]
        public string IpAddress { get; set; }

        [Required]
        [Column("yra_istrintas")]
        public bool IsDeleted { get; set; }

        [Column("fk_SKELBIMAIid")]
        public virtual ICollection<Notice> Notices { get; set; }

        /*[Column("fk_PARODYMAIid")]
        public virtual ICollection<Display> displays { get; set; }*/
    }
}
