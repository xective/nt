﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NTportal.Data.Entities
{
    [Table("PERZIURA")]
    public class View 
    {
        [Key]
        [Column("fk_PARODYMASid")]
        [ForeignKey("Display")]
        public int DisplayID { get; set; }
        public virtual Display Display { get; set; }
        [Required]
        [Column("trukme")]
        public int Duration { get; set; }
        [Required]
        [Column("ar_susisieke")]
        public bool DidContact { get; set; }
        [Required]
        [Column("sukurimo_data")]
        public DateTime createdOn { get; set; }
    }
}
