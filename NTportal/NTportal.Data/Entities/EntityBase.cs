﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.Data.Entities
{
    public abstract class EntityBase
    {
        [Key]
        public int id { get; set; }
        [Required]
        [Column("sukurimo_data")]
        public DateTime createdOn { get; set; }
    }
}
