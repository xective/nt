﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NTportal.Data.Entities
{
    [Table("ADRESAS")]
    public class Address : EntityBase
    {
        [Column("fk_user_id")]
        public virtual User user { get; set; }
        [Column("fk_Pastatas")]
        //[ForeignKey("Building")]
        //public int? BuildingID { get; set; }
        public virtual Building Building { get; set; }

        [Column("salis", TypeName = "varchar")]
        [MaxLength(20)]
        public string Country { get; set; }


        [Column("rajonas", TypeName = "varchar")]
        [MaxLength(20)]
        public string Region { get; set; }


        [Column("miestas", TypeName = "varchar")]
        [MaxLength(20)]
        public string City { get; set; }


        [Column("gatve", TypeName = "varchar")]
        [MaxLength(20)]
        public string Street { get; set; }


        [Column("pastato_numeris", TypeName = "varchar")]
        [MaxLength(10)]
        public string BuildingNumber { get; set; }
    }
}
