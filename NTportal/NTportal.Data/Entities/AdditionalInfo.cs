﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.Data.Entities
{
    [Table("VARTOTOJO_INFORMACIJA")]
    public class AdditionalInfo 
    {
        [Required]
        [Key]
        [ForeignKey("User")]
        [Column("fk_PRISIJUNGIMAS")]
        public int userID { get; set; }
        public virtual User User { get; set; }
        [Column("vardas", TypeName = "varchar")]
        [MaxLength(20)]
        public string FirstName { get; set; }

        [Column("pavarde", TypeName = "varchar")]
        [MaxLength(20)]
        public string LastName { get; set; }

        [Column("gimimo_data")]
        public DateTime BirthDate { get; set; }

        [Column("telefonas")]
        [MaxLength(20)]
        public string PhoneNumber { get; set; }
    }
}
