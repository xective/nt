﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.Data.Entities
{
    [Table("SKELBIMAS")]
    public class Notice : EntityBase
    {
        [Required]
        [Column("fk_PRISIJUNGIMASid")]
        public int userID { get; set; }
        public virtual User user { get; set; }
        [Required]
        [Column("skelbimo_tipas", TypeName = "varchar")]
        [MaxLength(20)]
        public string noticeType { get; set; }
        [Required]
        [Column("pavadinimas", TypeName = "varchar")]
        [MaxLength(40)]
        public string name { get; set; }
        [Required]
        [Column("miestas", TypeName = "varchar")]
        public string city { get; set; }
        [Required]
        [Column("aprasymas", TypeName = "varchar")]
        [MaxLength(300)]
        public string description { get; set; }
        [Required]

        [Column("tikslas")]
        public goals goal { get; set; }
        [Required]

        [Column("paskirtis")]    
        public purposes purpose { get; set; }
        [Required]
        [Column("kontaktinis_telefonas", TypeName = "varchar")]
        [MaxLength(20)]
        public string number { get; set; }
        [Column("kontaktinis_el_pastas", TypeName = "varchar")]
        [MaxLength(30)]
        public string email { get; set; }
        [Column("kaina")]
        public double? price { get; set; }
        [Required]
        [Column("zemes_plotas")]
        public double area { get; set; }
        [Column("zemes_kalnuotumas", TypeName = "varchar")]
        [MaxLength(30)]
        public string landMountainousness { get; set; }
        [Required]
        [Column("zemes_paskirtis", TypeName = "varchar")]
        [MaxLength(30)]
        public string landPurpose { get; set; }
        [Column("centro_ilguma")]
        public double centerLongitude { get; set; }
        [Column("centro_platuma")]
        public double centerLatitude { get; set; }


        [Required]
        [Column("fk_PASTATAI")]
        public virtual ICollection<Building> buildings { get; set; }
        //[Required]
        //[Column("fk_PASKELBIMAI")]
        //public virtual ICollection<Publishment> publishments { get; set; }
    }
    
    public enum goals { rent, sale, trade};
    public enum purposes { offer, seek}
}
