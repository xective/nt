﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NTportal.Data.Entities
{
    [Table("PIRMIENYBE")]
    public class Priority1 : EntityBase
    {
        [Required]
        [Column("fk_PASKELBIMAS")]
        public int publishmentID { get; set; }
        //public virtual Publishment publishment { get; set; }
        [Required]
        [Column("data_nuo")]
        public DateTime dateFrom { get; set; }
        [Required]
        [Column("data_iki")]
        public DateTime dateTo { get; set; }
        [Required]
        [Column("lygis")]
        public int level { get; set; }
    }
}
