﻿using NTportal.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.Data.DatabaseContext
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class MySqlDbContext : DbContext
    {
        public MySqlDbContext() : base("mysqlCon")
        {
            //Here we can add some data to db when we initialize db, for now we do not need it
            //Database.SetInitializer<MySqlDbContext>(new MySqlDbInitializer());
            Database.SetInitializer<MySqlDbContext>(new MySqlDbInitializer());
        }

        //This property define a table in our db
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<Display> Displays { get; set; }
        //public DbSet<Guest> Guests { get; set; }
        public DbSet<Notice> Notices { get; set; }
        //public DbSet<Priority> Priorities { get; set; }
        //public DbSet<Publishment> Publishments { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<View> Views { get; set; }
        public DbSet<AdditionalInfo> AddInfos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Building>()
                .HasRequired(et => et.Address);
                //.WithOptional(eo => eo.Building);
            modelBuilder.Entity<User>().HasOptional(x => x.Address)
                .WithOptionalPrincipal().Map(x => x.MapKey("fk_user_id"));
            modelBuilder.Entity<Address>().HasOptional(x => x.user)
                .WithOptionalPrincipal().Map(x => x.MapKey("fk_address_id"));
        }
    }


}
