﻿using NTportal.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.Data.DatabaseContext
{
    //public class MySqlDbInitializer : DropCreateDatabaseAlways<MySqlDbContext>
    // public class MySqlDbInitializer : MigrateDatabaseToLatestVersion<MySqlDbContext, Configuration>
    // public class MySqlDbInitializer : DropCreateDatabaseIfModelChanges<MySqlDbContext>
    // public class MySqlDbInitializer : CreateDatabaseIfNotExists<MySqlDbContext>
    public class MySqlDbInitializer : CreateDatabaseIfNotExists<MySqlDbContext>
    {
        protected override void Seed(MySqlDbContext context)
        {
            //User user1 = new User
            //{
            //    address = null,
            //    createdOn = DateTime.Now,
            //    role = "consumer",
            //    email = "fake@email.com",
            //    id = 1,
            //    ipAddress = "127.0.0.1",
            //    isDeleted = false,
            //    notices = new List<Notice>(),
            //    password = "windows123",
            //    username = "someUsr"
            //};
            //User user2 = new User
            //{
            //    address = null,
            //    createdOn = DateTime.Now,
            //    role = "other",
            //    email = "fakeTadis@email.com",
            //    id = 1,
            //    ipAddress = "127.0.0212.1",
            //    isDeleted = false,
            //    notices = new List<Notice>(),
            //    password = "bimba",
            //    username = "nenone"
            //};
            //User user3 = new User
            //{
            //    address = null,
            //    createdOn = DateTime.Now,
            //    role = "admin",
            //    email = "fakeTaxi@email.com",
            //    id = 2,
            //    ipAddress = "12777.0.0.1",
            //    isDeleted = false,
            //    notices = new List<Notice>(),
            //    password = "wiasdss777ndows123",
            //    username = "somfgfdseUsr"
            //};
            //user1.info = new AdditionalInfo { birthDate = new DateTime(1996, 1, 2), firstName = "NoName1", lastName = "NoSurname1", phone = "123456789", user = user1, createdOn = DateTime.Now };
            //User user2 = new User
            //{
            //    address = null,
            //    createdOn = DateTime.Now,
            //    displays = new List<Display>(),
            //    email = "fake2@email.com",
            //    id = 2,
            //    ipAddress = "",
            //    isDeleted = false,
            //    notices = new List<Notice>(),
            //    password = "admin2",
            //    username = "user2"
            //};
            //user2.info = new AdditionalInfo { birthDate = new DateTime(1996, 1, 3), firstName = "NoName2", lastName = "NoSurname2", phone = "234567891", user = user2, createdOn = DateTime.Now };
            //User user3 = new User
            //{
            //    address = null,
            //    createdOn = DateTime.Now,
            //    displays = new List<Display>(),
            //    email = "fake3@email.com",
            //    id = 3,
            //    ipAddress = "",
            //    isDeleted = false,
            //    notices = new List<Notice>(),
            //    password = "admin3",
            //    username = "user3"
            //};
            //user3.info = new AdditionalInfo { birthDate = new DateTime(1996, 1, 4), firstName = "NoName3", lastName = "NoSurname3", phone = "345678912", user = user3, createdOn = DateTime.Now };
            //context.Users.Add(user1);
            //context.Users.Add(user2);
            //context.Users.Add(user3);
            //context.Guests.Add(new Guest { createdOn = DateTime.Now, displays = new List<Display>(), id = 1, ip = "111.111.01.1", visitDate = DateTime.Now, visitDuration = 55 });
            //context.SaveChanges();
            context.SaveChanges();
        }
    }
}
