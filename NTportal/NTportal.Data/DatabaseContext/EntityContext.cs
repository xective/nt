﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Data.Entity;
//using System.Data.Entity.ModelConfiguration.Conventions;

//namespace NTportal.Data.Entities
//{
//    public class EntityContext : DbContext
//    {
//        public EntityContext() : base("EntityContext")
//        {
//        }

//        public DbSet<Address> Addresses { get; set; }
//        public DbSet<Building> Buildings { get; set; }
//        public DbSet<Display> Displays { get; set; }
//        public DbSet<Guest> Guests { get; set; }
//        public DbSet<Notice> Notices { get; set; }
//        public DbSet<Priority> Priorities { get; set; }
//        public DbSet<Publishment> Publishments { get; set; }
//        public DbSet<User> Users { get; set; }
//        public DbSet<View> views { get; set; }
//        protected override void OnModelCreating(DbModelBuilder modelBuilder)
//        {
//            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
//        }
//    }
//}
