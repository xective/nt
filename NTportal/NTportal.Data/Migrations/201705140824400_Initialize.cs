namespace NTportal.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initialize : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VARTOTOJO_INFORMACIJA",
                c => new
                    {
                        fk_PRISIJUNGIMAS = c.Int(nullable: false),
                        vardas = c.String(maxLength: 20, unicode: false),
                        pavarde = c.String(maxLength: 20, unicode: false),
                        gimimo_data = c.DateTime(nullable: false, precision: 0),
                        telefonas = c.String(maxLength: 20, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.fk_PRISIJUNGIMAS)
                .ForeignKey("dbo.PRISIJUNGIMAS", t => t.fk_PRISIJUNGIMAS)
                .Index(t => t.fk_PRISIJUNGIMAS);
            
            CreateTable(
                "dbo.PRISIJUNGIMAS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        slapyvardis = c.String(maxLength: 20, storeType: "nvarchar"),
                        slaptazodis = c.String(nullable: false, maxLength: 20, storeType: "nvarchar"),
                        prisijungimo_el_pastas = c.String(nullable: false, maxLength: 30, storeType: "nvarchar"),
                        role = c.String(nullable: false, maxLength: 20, storeType: "nvarchar"),
                        ip_adresas = c.String(nullable: false, maxLength: 30, storeType: "nvarchar"),
                        yra_istrintas = c.Boolean(nullable: false),
                        sukurimo_data = c.DateTime(nullable: false, precision: 0),
                        fk_address_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.ADRESAS", t => t.fk_address_id)
                .Index(t => t.fk_address_id);
            
            CreateTable(
                "dbo.ADRESAS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        salis = c.String(maxLength: 20, unicode: false),
                        rajonas = c.String(maxLength: 20, unicode: false),
                        miestas = c.String(maxLength: 20, unicode: false),
                        gatve = c.String(maxLength: 20, unicode: false),
                        pastato_numeris = c.String(maxLength: 10, unicode: false),
                        sukurimo_data = c.DateTime(nullable: false, precision: 0),
                        fk_user_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.PRISIJUNGIMAS", t => t.fk_user_id)
                .Index(t => t.fk_user_id);
            
            CreateTable(
                "dbo.PASTATAS",
                c => new
                    {
                        BuildingID = c.Int(nullable: false),
                        fk_ADRESAS = c.Int(nullable: false),
                        fk_SKELBIMAS = c.Int(nullable: false),
                        statymo_data = c.DateTime(nullable: false, precision: 0),
                        statymo_iki = c.DateTime(nullable: false, precision: 0),
                        bendras_plotas = c.Double(nullable: false),
                        aukstu_skaicius = c.Int(nullable: false),
                        patogumai = c.String(maxLength: 255, unicode: false),
                        isplanavimas = c.String(maxLength: 100, unicode: false),
                        paskirtis = c.String(nullable: false, maxLength: 30, unicode: false),
                        ar_yra_vadentekis = c.Boolean(nullable: false),
                        ar_yra_kanalizacija = c.Boolean(nullable: false),
                        bukle = c.Int(nullable: false),
                        kategorija = c.Int(nullable: false),
                        tipas = c.Int(nullable: false),
                        sildymas = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BuildingID)
                .ForeignKey("dbo.ADRESAS", t => t.BuildingID)
                .ForeignKey("dbo.SKELBIMAS", t => t.fk_SKELBIMAS, cascadeDelete: true)
                .Index(t => t.BuildingID)
                .Index(t => t.fk_SKELBIMAS);
            
            CreateTable(
                "dbo.SKELBIMAS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        fk_PRISIJUNGIMASid = c.Int(nullable: false),
                        skelbimo_tipas = c.String(nullable: false, maxLength: 20, unicode: false),
                        pavadinimas = c.String(nullable: false, maxLength: 40, unicode: false),
                        aprasymas = c.String(nullable: false, maxLength: 300, unicode: false),
                        tikslas = c.Int(nullable: false),
                        paskirtis = c.Int(nullable: false),
                        kontaktinis_telefonas = c.String(nullable: false, maxLength: 20, unicode: false),
                        kontaktinis_el_pastas = c.String(maxLength: 30, unicode: false),
                        kaina = c.Double(),
                        zemes_plotas = c.Double(nullable: false),
                        zemes_kalnuotumas = c.String(maxLength: 30, unicode: false),
                        zemes_paskirtis = c.String(nullable: false, maxLength: 30, unicode: false),
                        centro_ilguma = c.Double(nullable: false),
                        centro_platuma = c.Double(nullable: false),
                        sukurimo_data = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.PRISIJUNGIMAS", t => t.fk_PRISIJUNGIMASid, cascadeDelete: true)
                .Index(t => t.fk_PRISIJUNGIMASid);
            
            CreateTable(
                "dbo.PARODYMAS",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        fk_PASKELBIMASid = c.Int(nullable: false),
                        ar_yra_vartotojas = c.Boolean(nullable: false),
                        fk_guest_ID = c.Int(),
                        fk_user_ID = c.Int(),
                        sukurimo_data = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.PERZIURA",
                c => new
                    {
                        fk_PARODYMASid = c.Int(nullable: false),
                        trukme = c.Int(nullable: false),
                        ar_susisieke = c.Boolean(nullable: false),
                        sukurimo_data = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.fk_PARODYMASid)
                .ForeignKey("dbo.PARODYMAS", t => t.fk_PARODYMASid)
                .Index(t => t.fk_PARODYMASid);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PERZIURA", "fk_PARODYMASid", "dbo.PARODYMAS");
            DropForeignKey("dbo.VARTOTOJO_INFORMACIJA", "fk_PRISIJUNGIMAS", "dbo.PRISIJUNGIMAS");
            DropForeignKey("dbo.ADRESAS", "fk_user_id", "dbo.PRISIJUNGIMAS");
            DropForeignKey("dbo.PRISIJUNGIMAS", "fk_address_id", "dbo.ADRESAS");
            DropForeignKey("dbo.SKELBIMAS", "fk_PRISIJUNGIMASid", "dbo.PRISIJUNGIMAS");
            DropForeignKey("dbo.PASTATAS", "fk_SKELBIMAS", "dbo.SKELBIMAS");
            DropForeignKey("dbo.PASTATAS", "BuildingID", "dbo.ADRESAS");
            DropIndex("dbo.PERZIURA", new[] { "fk_PARODYMASid" });
            DropIndex("dbo.SKELBIMAS", new[] { "fk_PRISIJUNGIMASid" });
            DropIndex("dbo.PASTATAS", new[] { "fk_SKELBIMAS" });
            DropIndex("dbo.PASTATAS", new[] { "BuildingID" });
            DropIndex("dbo.ADRESAS", new[] { "fk_user_id" });
            DropIndex("dbo.PRISIJUNGIMAS", new[] { "fk_address_id" });
            DropIndex("dbo.VARTOTOJO_INFORMACIJA", new[] { "fk_PRISIJUNGIMAS" });
            DropTable("dbo.PERZIURA");
            DropTable("dbo.PARODYMAS");
            DropTable("dbo.SKELBIMAS");
            DropTable("dbo.PASTATAS");
            DropTable("dbo.ADRESAS");
            DropTable("dbo.PRISIJUNGIMAS");
            DropTable("dbo.VARTOTOJO_INFORMACIJA");
        }
    }
}
