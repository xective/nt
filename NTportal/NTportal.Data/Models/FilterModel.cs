﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NTportal.Data.Models
{
    public class FilterModel
    {
        public string Name { get; set; }
        public string City { get; set; }
        public string Type { get; set; }
        public int Goal { get; set; }
        public int RoomCountFrom { get; set; }
        public int RoomCountTo { get; set; }
        public double PriceFrom { get; set; }
        public double PriceTo { get; set; }
        public double FlatSizeFrom { get; set; }
        public double FlatSizeTo { get; set; }
        public double SizeFrom { get; set; }
        public double SizeTo { get; set; }
        public int FloorFrom { get; set; }
        public int FloorTo { get; set; }
    }
}
