﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTportal.Data.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace NTportal.Data.Models
{
    public class BuildingModel
    {
        public int BuildingID { get; set; }
        public virtual Address Address { get; set; }
        public virtual Notice Notice { get; set; }
        public DateTime ConstructionFrom { get; set; }
        public DateTime ConstructionTo { get; set; }
        public double TotalArea { get; set; }
        public int Floors { get; set; }
        public int Rooms { get; set; }
        public string Conveniences { get; set; }
        public string Layout { get; set; }
        public string Purpose { get; set; }
        public bool HasWater { get; set; }
        public bool HasPlumbing { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public conditions Condition { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public categories Category { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public types Type { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public heatingType Heating { get; set; }

        public static explicit operator BuildingModel(Building b) {
            return new BuildingModel {
                Address = b.Address,
                BuildingID = b.BuildingID,
                Category = b.category,
                Condition = b.condition,
                ConstructionFrom = b.constructionFrom,
                ConstructionTo = b.constructionTo,
                Conveniences = b.conveniences,
                Floors = b.floors,
                HasPlumbing = b.hasPlumbing,
                HasWater = b.hasWater,
                Heating = b.heating,
                Layout = b.layout,
                Notice = b.notice,
                Purpose = b.purpose,
                Rooms = b.rooms,
                TotalArea = b.totalArea,
                Type = b.type
            };
        }

        public static explicit operator Building(BuildingModel b) {
            return new Building {
                Address = b.Address,
                addressID = b.Address.id,
                BuildingID = b.BuildingID,
                category = b.Category,
                condition = b.Condition,
                constructionFrom = b.ConstructionFrom,
                constructionTo = b.ConstructionTo,
                conveniences = b.Conveniences,
                floors = b.Floors,
                hasPlumbing = b.HasPlumbing,
                hasWater = b.HasWater,
                heating = b.Heating,
                layout = b.Layout,
                notice = b.Notice,
                noticeID = b.Notice.id,
                purpose = b.Purpose,
                rooms = b.Rooms,
                totalArea = b.TotalArea,
                type = b.Type
            };
        }
    }
}
