﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTportal.Data.Entities;

namespace NTportal.Data.Models
{
    public class UserModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public string UserName { get; set; }
        public string IP { get; set; }
        
        public static explicit operator UserModel(User v)
        {
            return new UserModel {
                Email = v.Email,
                Password = v.Password,
                PhoneNumber = v.Password,
                City = (v.Address == null ? null : v.Address.City  )
            };
        }
        public static explicit operator User(UserModel v)
        {
            return new User
            {
                Email = v.Email,
                Password = v.Password,
                Username = v.UserName,
                IpAddress = v.IP,
                Role = "Consumer",
                IsDeleted = false,
                createdOn = DateTime.Now
                //City = (v.Address == null ? null : v.Address.city)
            };
        }
    }
}
