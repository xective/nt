﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTportal.Data.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace NTportal.Data.Models
{
    public class NoticeModel
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public User User { get; set; }
        public string NoticeType { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Description { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public goals Goal { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public purposes Purpose { get; set; }
        public string Number { get; set; }
        public string Email { get; set; }
        public double? Price { get; set; }
        public double Area { get; set; }
        public string LandMountainousness { get; set; }
        public string LandPurpose { get; set; }
        public double CenterLongitude { get; set; }
        public double CenterLatitude { get; set; }
        public virtual ICollection<Building> Buildings { get; set; }

        public static explicit operator NoticeModel(Notice n)
        {
            return new NoticeModel {
                Area = n.area,
                Buildings = n.buildings,
                CenterLatitude = n.centerLatitude,
                CenterLongitude = n.centerLongitude,
                City = n.city,
                CreatedOn = n.createdOn,
                Description = n.description,
                Email = n.email,
                Goal = n.goal,
                Id = n.id,
                LandMountainousness = n.landMountainousness,
                LandPurpose = n.landPurpose,
                Name = n.name,
                NoticeType = n.noticeType,
                Number = n.number,
                Price = n.price,
                Purpose = n.purpose,
                User = n.user
            };
        }

        public static explicit operator Notice(NoticeModel n) {
            return new Notice {
                area = n.Area,
                buildings = n.Buildings,
                centerLatitude = n.CenterLatitude,
                centerLongitude = n.CenterLongitude,
                city = n.City,
                createdOn = n.CreatedOn,
                description = n.Description,
                email = n.Email,
                goal = n.Goal,
                id = n.Id,
                landMountainousness = n.LandMountainousness,
                landPurpose = n.LandPurpose,
                name = n.Name,
                noticeType = n.NoticeType,
                number = n.Number,
                price = n.Price,
                purpose = n.Purpose,
                user = n.User
        };
        }
    }
}
