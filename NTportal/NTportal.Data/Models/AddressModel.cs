﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NTportal.Data.Entities;

namespace NTportal.Data.Models
{
    public class AddressModel
    {
        public int Id { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string BuildingNumber { get; set; }
        public DateTime CreatedOn { get; set; }
        public User User { get; set; }

        public static explicit operator AddressModel(Address a)
        {
            return new AddressModel
            {
                Id = a.id,
                Country = a.Country,
                Region = a.Region,
                City = a.City,
                Street = a.Street,
                BuildingNumber = a.BuildingNumber,
                CreatedOn = a.createdOn,
                User = a.user
            };
        }

        public static explicit operator Address(AddressModel a)
        {
            return new Address
            {
                id = a.Id,
                Country = a.Country,
                Region = a.Region,
                City = a.City,
                Street = a.Street,
                BuildingNumber = a.BuildingNumber,
                createdOn = a.CreatedOn,
                user = a.User
            };
        }
    }
}
